﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sss.Models
{
    public class wtUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Season { get; set; }
        public string Company { get; set; }
        public string CompanyName { get; set; }
        public string CompanyInfo { get; set; }
        public string LoginDT { get; set; }
        public string uMail { get; set; }
        public string LookingAtTicket { get; set; }


        public wtUser() { }

        public wtUser(int id, string name, string season, string company, 
                      string companyname, string companyinfo, string logindt, 
                      string umail, string lookingatticket )
        {
            this.Id = id;
            this.Name = name;
            this.Season = season;
            this.Company = company;
            this.CompanyName = companyname;
            this.CompanyInfo = companyinfo;
            this.LoginDT = logindt;
            this.uMail = umail;
            this.LookingAtTicket = lookingatticket;
        }
    }
}
