﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sss.Models
{
    public class wtSeller
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<wtCustomer> Customers{ get; set; }
        public List<wtTktSeries> TktSeries{ get; set; }

        public wtSeller() { }

        public wtSeller(int id, string name, List< wtCustomer> customers, List< wtTktSeries> tktseries)    
    {
            this.Id = id;
            this.Name = name;
            this.Customers = customers;
            this.TktSeries = tktseries;
        }
    }

    public class wtTktSeries
    {
        public int Auto { get; set; }
        public string Code { get; set; }

        public wtTktSeries() { }

        public wtTktSeries(int auto, string code)
        {
            this.Auto = auto;
            this.Code = code;
        }
    }
}
