﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sss.Models
{
    public class wtCustomer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public wtLanguage Language { get; set; }
        public wtMarket Market { get; set; }

        public wtCustomer() { }

        public wtCustomer(int id, string name, wtLanguage language, wtMarket market)
        {
            this.Id = id;
            this.Name = name;
            this.Language = language;
            this.Market = market;
        }
    }

    public class wtMarket
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public wtMarket() { }

        public wtMarket(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
    }

    public class wtLanguage
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public wtLanguage() { }

        public wtLanguage(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
    }
}
