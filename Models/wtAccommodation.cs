﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sss.Models
{
    public class wtAccommodation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public wtPuPoint PuPoint { get; set; }
    
    public wtAccommodation(){}

    public wtAccommodation(int id, string name, wtPuPoint pupoint)
        {
            this.Id=id;
            this.Name=name;
            this.PuPoint = pupoint;
        }
    }

    public class wtPuPoint
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PzId { get; set; }

        public wtPuPoint() { }

        public wtPuPoint(int id, string name, int pzip)
        {
            this.Id = id;
            this.Name = name;
            this.PzId = pzip;
        }
    }
}
