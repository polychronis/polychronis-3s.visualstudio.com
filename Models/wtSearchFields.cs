﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace sss.Models
{
    public class wtSearchFields
    {
        public string Company { get; set; }
        public string Season { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int DateBasis { get; set; }
        public int Seller { get; set; }
        public int Customer { get; set; }
        public int Tour { get; set; }
        public int Point { get; set; }
        public int Accommodation { get; set; }
        public string Client { get; set; }

        public wtSearchFields() { }

        public wtSearchFields(string company, string season, DateTime dateFrom, DateTime dateTo, int dateBasis,
             int seller, int customer, int tour, int point, int accommodation, string client )
        {
            this.Company = company;
            this.Season = season;
            this.DateFrom = dateFrom;
            this.DateTo = dateTo;
            this.DateBasis = dateBasis;
            this.Seller = seller;
            this.Customer = customer;
            this.Tour = tour;
            this.Point = point;
            this.Accommodation = accommodation;
            this.Client = client;
        }

        public string MakeRequestString()
        {
           
            String s = "<?xml version=\"1.0\" ?>";
            s += "<aRequest request=\"WEBOTR BOOKINGS LIST\">";
            s += "<company>" + Company + "</company>";
            s += "<season>" + Season + "</season>";
            s += "<from>" + DateFrom.ToString("dd/MM/yyyy") + "</from>";
            s += "<to>" + DateTo.ToString("dd/MM/yyyy") + "</to>";
            s += "<datesBasis>" + DateBasis.ToString() + "</datesBasis>";
            s += "<seller>" + Seller + "</seller>";
            s += "<customer>" + Customer + "</customer>";
            s += "<tour>" + Tour + "</tour>";
            s += "<point>" + Point + "</point>";
            s += "<accommodation>" + Accommodation + "</accommodation>";
            s += "<client>" + Client + "</client>";

            s += "</aRequest>";

            return s;
        }
    }
}
