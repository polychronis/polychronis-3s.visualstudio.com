﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sss.Models
{
    public class wtTour
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ssDay { get; set; }
        public string ssTime { get; set; }
        public List<wtTourSchedule> Schedules { get; set; }
        public List<wtTourTimeTable> TimeTables { get; set; }
        public List<wtRate> Rates { get; set; }
        public int Override { get; set; }

        public wtTour() { }

        public wtTour(int id, string name, string ssday, string sstime,
                      List<wtTourSchedule> schedules, List<wtTourTimeTable> timetables, 
                      List<wtRate> rates, int overide)    
    {
            this.Id = id;
            this.Name = name;
            this.ssDay = ssday;
            this.ssTime = sstime;
            this.Schedules = schedules;
            this.TimeTables = timetables;
            this.Rates = rates;
            this.Override = overide;

        }
    }

    public class wtTourSchedule
    {
        public string DtFrom { get; set; }
        public string DtTo { get; set; }
        public wtMarket Market { get; set; }
        public Dictionary<int, int> D { get; set; }

        public wtTourSchedule() { }

        public wtTourSchedule(string dtfrom, string dtto, wtMarket market, Dictionary<int, int> d)
        {
            this.DtFrom = dtfrom;
            this.DtTo = dtto;
            this.Market = market;
            this.D = d;
        }
    }

    public class wtTourTimeTable
    {
        public int Id { get; set; }
        public string DtFrom { get; set; }
        public string DtTo { get; set; }
        public wtMarket Market { get; set; }
        public List<wtTimeTablePUT> PuTimes { get; set; }

        public wtTourTimeTable() { }

        public wtTourTimeTable(int id, string dtfrom, string dtto, wtMarket market, List<wtTimeTablePUT> putimes)
        {
            this.Id = id;
            this.DtFrom = dtfrom;
            this.DtTo = dtto;
            this.Market = market;
            this.PuTimes = putimes;
        }
    }


    public class wtTimeTablePUT
    {
        public string Time { get; set; }
        public int PointId { get; set; }

        public wtTimeTablePUT() { }

        public wtTimeTablePUT(string time, int pointid)
        {
            this.Time = time;
            this.PointId = pointid;
        }
    }

}
