﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Printing;
using System.Drawing;
using sss.Tools;
using System.Globalization;
using System.IO;


namespace sss.Models
{
    public class wtBooking
    {
        public int Id { get; set; }
        public string AdAddSuppl { get; set; }
        public int Ad { get; set; }
        public decimal AdRate { get; set; }
        public string AdTsfSuppl { get; set; }
        public wtAccommodation Accommodation { get; set; }
        public string Customer { get; set; }
        public wtLanguage Language { get; set; }
        public wtMarket Market { get; set; }
        public wtUser User { get; set; }
        public string Notes { get; set; }
        public string ChAddSuppl { get; set; }
        public int Ch { get; set; }
        public string Client { get; set; }
        public decimal ChRate { get; set; }
        public string ChTsfSuppl { get; set; }
        public wtPuPoint PuPoint { get; set; }
        public wtSeller Seller { get; set; }
        public wtTour Tour { get; set; }
        public string PuTime { get; set; }
        public string RateCode { get; set; }
        public string Room { get; set; }
        public string TourDate { get; set; }
        public string TktNo { get; set; }
        public string TktSeries { get; set; }
        public string Season { get; set; }
        public string IsOverride { get; set; }
        public decimal TotAmount { get; set; }

        public decimal AdAmount { get; set; }
        public string AdFree { get; set; }
        public string AdPrice { get; set; }
        public string AgentName { get; set; }
        public string Amount { get; set; }
        public decimal ChAmount { get; set; }
        public string ChFree { get; set; }
        public string ChPrice { get; set; }
        public string EntryDate { get; set; }
        public string EntryTime { get; set; }

        public string UserName { get; set; }


        public wtBooking() { }

        //public wtBooking(int _id, string _adAddSuppl, int _ad, decimal _adRate, string _adTsfSuppl, wtAccommodation _accommodation,
        //     string _customer, wtLanguage _language, wtMarket _market, wtUser _user, string _notes,
        //     string _chAddSuppl, int _ch, string _client, decimal _chRate, string _chTsfSuppl,
        //     wtPuPoint _pupoint, wtSeller _seller, wtTour _tour, string _puTime, string _rateCode,
        //     string _room, string _tourDate, string _tktNo, string _tktSeries, string _season,
        //     string _isoverride, decimal _totAmount,
        //     decimal _adAmount, string _adFree, string _adPrice, string _agentName, decimal _chAmount, string _amount,
        //     string _chFree, string _chPrice, string _entryDate, string _entryTime, string _pointName,
        //     string _userName)
        //{
        //    this.Id = _id;
        //    this.AdAddSuppl = _adAddSuppl;
        //    this.Ad = _ad;
        //    this.AdRate = _adRate;
        //    this.AdTsfSuppl = _adTsfSuppl;
        //    this.Accommodation = _accommodation;
        //    this.Customer = _customer;
        //    this.Language = _language;
        //    this.Market = _market;
        //    this.User = _user;
        //    this.Notes = _notes;
        //    this.ChAddSuppl = _chAddSuppl;
        //    this.Ch = _ch;
        //    this.Client = _client;
        //    this.ChRate = _chRate;
        //    this.ChTsfSuppl = _chTsfSuppl;
        //    this.PuPoint = _pupoint;
        //    this.Seller = _seller;
        //    this.Tour = _tour;
        //    this.PuTime = _puTime;
        //    this.RateCode = _rateCode;
        //    this.Room = _room;
        //    this.TourDate = _tourDate;
        //    this.TktNo = _tktNo;
        //    this.TktSeries = _tktSeries;
        //    this.Season = _season;
        //    this.IsOverride = _isoverride;
        //    this.TotAmount = _totAmount;

        //    this.AdAmount = _adAmount;
        //    this.AdFree = _adFree;
        //    this.AdPrice = _adPrice;
        //    this.AgentName = _agentName;
        //    this.Amount = _amount;
        //    this.ChAmount = _chAmount;
        //    this.ChFree = _chFree;
        //    this.ChPrice = _chPrice;
        //    this.EntryDate = _entryDate;
        //    this.EntryTime = _entryTime;


        //    this.UserName = _userName;
        //}

        public string MakeRequestString()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("el-GR");

            String s = "<?xml version=\"1.0\" ?>";
            s += "<aRequest request=\"WEBOTR NEW BOOKING\">";
            s += "<adAddSuppl></adAddSuppl>";
            s += "<ad>" + Ad.ToString() + "</ad>";
            s += "<adRate>" + AdRate.ToString(CultureInfo.GetCultureInfo("en")) + "</adRate>";
            s += "<adTsfSuppl></adTsfSuppl>";
            s += "<accommodation>" + Accommodation.Id + "</accommodation>";
            s += "<company>" + User.Company + "</company>";
            s += "<customer>" + Customer + "</customer>";
            s += "<language>" + Language.Id.ToString() + "</language>";
            s += "<market>" + Market.Id.ToString() + "</market>";
            s += "<user>" + User.Id.ToString() + "</user>";
            s += "<notes>" + Notes + "</notes>";
            s += "<chAddSuppl>" + "" + "</chAddSuppl>";
            s += "<ch>" + Ch.ToString() + "</ch>";
            s += "<client>" + Client + "</client>";
            s += "<chRate>" + ChRate.ToString(CultureInfo.GetCultureInfo("en")) + "</chRate>";
            s += "<chTsfSuppl></chTsfSuppl>";
            s += "<point>" + PuPoint.Id.ToString() + "</point>";
            s += "<seller>" + Seller.Id.ToString() + "</seller>";
            s += "<tour>" + Tour.Id.ToString() + "</tour>";
            s += "<puTime>" + PuTime + "</puTime>";
            s += "<rateCode></rateCode>";
            s += "<room>" + Room + "</room>";
            s += "<tourDate>" + TourDate + "</tourDate>";
            s += "<tktNo>" + TktNo + "</tktNo>";
            s += "<tktSeries>" + TktSeries + "</tktSeries>";
            s += "<season>" + Season + "</season>";
            s += "<override>" + IsOverride + "</override>";
            s += "<totAmount>" + TotAmount.ToString(CultureInfo.GetCultureInfo("en")) + "</totAmount>";
            s += "</aRequest>";

            return s;
        }

        public void PrintTicket(PrinterSettings ps)
        {

            PrintDocument pdoc = new PrintDocument();
            pdoc.PrinterSettings = ps;



            pdoc.PrintPage += new PrintPageEventHandler(pdoc_PrintPage);
            pdoc.Print();
        }

        void pdoc_PrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;

            IniFile inifile = new IniFile("c:\\3S Tours\\tours.ini");

            int paperwidth = Convert.ToInt16(inifile.IniReadValue("printer", "paperwidth"));
            //int paperheight = Convert.ToInt16(inifile.IniReadValue("printer", "paperheight"));

            int fontsize = Convert.ToInt16(inifile.IniReadValue("printer", "fontsize"));
            int margins = Convert.ToInt16(inifile.IniReadValue("printer", "margins"));

            //e.MarginBounds.Size = new Size(margins, margins);

            int charactersOnPage = 0;
            int linesPerPage = 0;

            Font font_normal = new Font("Arial", fontsize);
            Font font_bold = new Font("Arial", fontsize, FontStyle.Bold);
            float fontHeight = font_normal.GetHeight();

            SolidBrush color = new SolidBrush(Color.Black);


            StringFormat align_left = new StringFormat();
            align_left.Alignment = StringAlignment.Near;
            //align_left.LineAlignment = StringAlignment.Near;

            StringFormat align_right = new StringFormat();
            align_right.Alignment = StringAlignment.Far;
            //align_right.LineAlignment = StringAlignment.Far;

            string underLine = "-------------------------------------------------------------------------";
            underLine = underLine + underLine;

            string label = "";
            string label2 = "";

            int startX = margins;
            int startY = margins;
            int Offset = 0;
            float bottomMargin = fontHeight + 3;

            RectangleF rect1 = new RectangleF(startX, startY + Offset, paperwidth - 2 * startX, fontHeight);


            graphics.DrawString("For the company", font_bold, color, rect1, align_right);
            rect1.Y = rect1.Y + bottomMargin;


            try
            {
                int counter = 0;
                // Read the file and display it line by line.
                System.IO.StreamReader header = new System.IO.StreamReader("c:\\3S Tours\\header.txt", Encoding.GetEncoding(1253));

                //int charactersOnPage = 0;
                //int linesPerPage = 0;

                while ((label = header.ReadLine()) != null)
                {
                    graphics.MeasureString(label, font_bold,
                        //e.MarginBounds.Size, StringFormat.GenericTypographic,
                                   new Size(paperwidth - 2 * startX, e.MarginBounds.Height),
                                   StringFormat.GenericTypographic,
                                   out charactersOnPage, out linesPerPage);
                    rect1.Height = (bottomMargin) * linesPerPage;
                    //* linesPerPage
                    /// (papersize - 2 * startX);

                    graphics.DrawString(label, font_bold, color, rect1, align_left);
                    rect1.Y = rect1.Y + rect1.Height;
                    counter++;
                }
            }
            catch { }

            graphics.DrawString(User.CompanyName, font_bold, color, rect1, align_left);
            rect1.Y = rect1.Y + bottomMargin;

            string[] cInfo = User.CompanyInfo.Split(new string[] { "\\n" }, StringSplitOptions.None);
            foreach (string s in cInfo)
            {
                graphics.DrawString(s, font_normal, color, rect1, align_left);
                rect1.Y = rect1.Y + bottomMargin;
            }

            rect1.Y = rect1.Y + bottomMargin;

            label = "Ticket: ";
            label2 = this.TktSeries + " " + this.TktNo;
            graphics.DrawString(label, font_normal, color, rect1, align_left);

            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Tour: ";
            label2 = this.Tour.Name;
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Date: ";
            label2 = this.TourDate;
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Pick up: ";
            label2 = this.PuTime + " " + this.PuPoint.Name; ;
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Accommodation: ";
            label2 = this.Accommodation.Name;
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Name: ";
            label2 = this.Client;
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Adults: ";
            label2 = this.Ad.ToString() + " x " + this.AdRate.ToString() + " = " + (this.Ad * this.AdRate).ToString();
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Children: ";
            label2 = this.Ch.ToString() + " x " + this.ChRate.ToString() + " = " + (this.Ch * this.ChRate).ToString();
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Final Price: ";
            label2 = this.TotAmount.ToString();
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            graphics.DrawString(this.Notes, font_normal, color, rect1, align_left);
            rect1.Y = rect1.Y + bottomMargin;

            label = "Booked on: ";
            label2 = this.EntryDate;
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Seller: ";
            label2 = this.Seller.Name;
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;


            try
            {
                int counter = 0;
                // Read the file and display it line by line.
                System.IO.StreamReader footer_seller = new System.IO.StreamReader("c:\\3S Tours\\footer_seller.txt", Encoding.GetEncoding(1253));

                //int charactersOnPage = 0;
                //int linesPerPage = 0;

                while ((label = footer_seller.ReadLine()) != null)
                {
                    graphics.MeasureString(label, font_normal,
                        //e.MarginBounds.Size, StringFormat.GenericTypographic,
                                   new Size(paperwidth - 2 * startX, e.MarginBounds.Height),
                                   StringFormat.GenericTypographic,
                                   out charactersOnPage, out linesPerPage);
                    rect1.Height = (bottomMargin) * linesPerPage;
                    //* linesPerPage
                    /// (papersize - 2 * startX);

                    graphics.DrawString(label, font_normal, color, rect1, align_left);
                    rect1.Y = rect1.Y + rect1.Height;
                    counter++;
                }
            }
            catch { }

            rect1.Y = rect1.Y + 4 * bottomMargin;
            rect1.Height = Convert.ToInt16(fontHeight);
            graphics.DrawString(underLine, font_normal, color, rect1, align_left);
            rect1.Y = rect1.Y + 6 * bottomMargin;
            //rect1.Y = rect1.Y + bottomMargin;

            //client copy
            graphics.DrawString("For the client", font_bold, color, rect1, align_right);
            rect1.Y = rect1.Y + bottomMargin;


            try
            {
                int counter = 0;
                // Read the file and display it line by line.
                System.IO.StreamReader header = new System.IO.StreamReader("c:\\3S Tours\\header.txt", Encoding.GetEncoding(1253));

                //int charactersOnPage = 0;
                //int linesPerPage = 0;

                while ((label = header.ReadLine()) != null)
                {
                    graphics.MeasureString(label, font_normal,
                        //e.MarginBounds.Size, StringFormat.GenericTypographic,
                                   new Size(paperwidth - 2 * startX, e.MarginBounds.Height),
                                   StringFormat.GenericTypographic,
                                   out charactersOnPage, out linesPerPage);
                    rect1.Height = (bottomMargin) * linesPerPage;
                    //* linesPerPage
                    /// (papersize - 2 * startX);

                    graphics.DrawString(label, font_bold, color, rect1, align_left);
                    rect1.Y = rect1.Y + rect1.Height;
                    counter++;
                }
            }
            catch { }


            graphics.DrawString(User.CompanyName, font_bold, color, rect1, align_left);
            rect1.Y = rect1.Y + bottomMargin;

            foreach (string s in cInfo)
            {
                graphics.DrawString(s, font_normal, color, rect1, align_left);
                rect1.Y = rect1.Y + bottomMargin;
            }

            rect1.Y = rect1.Y + bottomMargin;

            label = "Ticket: ";
            label2 = this.TktSeries + " " + this.TktNo;
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Tour: ";
            label2 = this.Tour.Name;
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Date: ";
            label2 = this.TourDate;
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Pick up: ";
            label2 = this.PuTime + " " + this.PuPoint.Name;
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Accommodation: ";
            label2 = this.Accommodation.Name;
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Name: ";
            label2 = this.Client;
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Adults: ";
            label2 = this.Ad.ToString() + " x " + this.AdRate.ToString() + " = " + (this.Ad * this.AdRate).ToString();
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Children: ";
            label2 = this.Ch.ToString() + " x " + this.ChRate.ToString() + " = " + (this.Ch * this.ChRate).ToString();
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Final Price: ";
            label2 = this.TotAmount.ToString();
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            graphics.DrawString(this.Notes, font_normal, color, rect1, align_left);
            rect1.Y = rect1.Y + bottomMargin;

            label = "Booked on: ";
            label2 = this.EntryDate;
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            label = "Seller: ";
            label2 = this.Seller.Name;
            graphics.DrawString(label, font_normal, color, rect1, align_left);
            rect1.X = rect1.X + Convert.ToInt16(graphics.MeasureString(label, font_normal).Width);
            graphics.DrawString(label2, font_bold, color, rect1, align_left);
            rect1.X = startX;
            rect1.Y = rect1.Y + bottomMargin;

            rect1.Y = rect1.Y + bottomMargin;

            try
            {
                int counter = 0;
                // Read the file and display it line by line.
                System.IO.StreamReader footer_client = new System.IO.StreamReader("c:\\3S Tours\\footer_client.txt", Encoding.GetEncoding(1253));

                //int charactersOnPage = 0;
                //int linesPerPage = 0;

                while ((label = footer_client.ReadLine()) != null)
                {
                    graphics.MeasureString(label, font_normal,
                        //e.MarginBounds.Size, StringFormat.GenericTypographic,
                                   new Size(paperwidth - 2 * startX, e.MarginBounds.Height),
                                   StringFormat.GenericTypographic,
                                   out charactersOnPage, out linesPerPage);
                    rect1.Height = (bottomMargin) * linesPerPage;
                    //* linesPerPage
                    /// (papersize - 2 * startX);

                    graphics.DrawString(label, font_normal, color, rect1, align_left);
                    rect1.Y = rect1.Y + rect1.Height;
                    counter++;
                }
            }
            catch { }
        }
    }
}
