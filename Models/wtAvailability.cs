﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace sss.Models
{
    //public class wtPlan
    //{
    //    public string Name { get; set; }
    //    public Dictionary<int, string> Quantity { get; set; }

    //    public wtPlan() { }

    //    public wtPlan(string name, Dictionary<int, string> quantity)
    //    {
    //        this.Name = name;
    //        this.Quantity = quantity;
    //    }
    //}

    //public class wtAvailability
    //{
    //    public Dictionary<int, string> Dates { get; set; }
    //    public List<wtPlan> Availability { get; set; }

    //    public wtAvailability() { }

    //    public wtAvailability(Dictionary<int, string> dates, List<wtPlan> availability)
    //    {
    //        this.Dates = dates;
    //        this.Availability = availability;
    //    }

    //    public string MakeRequestString(string season, DateTime tourdate)
    //    {
    //        String s = "<?xml version=\"1.0\" ?>";
    //        s += "<aRequest request=\"WEBOTR AVAILABILITY LIST\">";
    //        s += "<season>" + season + "</season>";
    //        s += "<tourDate>" + tourdate.ToString("d/M/yyyy") + "</tourDate>";
    //        s += "</aRequest>";

    //        return s;
    //    }
    //}

    public class wtSchedule
    {
        public string Day1 { get; set; }
        public string Day2 { get; set; }
        public string Day3 { get; set; }
        public string Day4 { get; set; }
        public string Day5 { get; set; }
        public string Day6 { get; set; }
        public string Day7 { get; set; }
       
             
        public wtSchedule() { }

        public wtSchedule(string d1, string d2, string d3, string d4, string d5, string d6, string d7)
        {
            this.Day1 = d1;
            this.Day2 = d2;
            this.Day3 = d3;
            this.Day4 = d4;
            this.Day5 = d5;
            this.Day6 = d6;
            this.Day7 = d7;


        }

    }

    

    public class wtAvailability
    {
        public string TourName { get; set; }
        public string Availability1 { get; set; }
        public string Availability2 { get; set; }
        public string Availability3 { get; set; }
        public string Availability4 { get; set; }
        public string Availability5 { get; set; }
        public string Availability6 { get; set; }
        public string Availability7 { get; set; }

        public wtAvailability() {}

        public wtAvailability(string tourname, string a1, string a2, string a3, string a4, string a5, string a6, string a7)
        {
            this.TourName = tourname;
            this.Availability1 = a1;
            this.Availability2 = a2;
            this.Availability3 = a3;
            this.Availability4 = a4;
            this.Availability5 = a5;
            this.Availability6 = a6;
            this.Availability7 = a7;
        }


  
   }
}
