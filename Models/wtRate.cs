﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sss.Models
{
    public class wtRate
    {
        public string DtFrom { get; set; }
        public string DtTo { get; set; }
        public decimal adTsfSuppl { get; set; }
        public decimal adAddSuppl { get; set; }
        public decimal chTsfSuppl { get; set; }
        public decimal chAddSuppl { get; set; }
        public int CustomerId { get; set; }
        public int PriceZone { get; set; }
        public string RateCode { get; set; }
        public decimal adRate { get; set; }
        public decimal chRate { get; set; }

        public wtRate() { }

        public wtRate(string dtfrom, string dtto, decimal adtsf, decimal adadd, decimal chtsf, decimal chadd,
                      int customerid, int pricezone, string ratecode, decimal adrate, decimal chrate)
        {
            this.DtFrom = dtfrom;
            this.DtTo = dtto;
            this.adTsfSuppl = adtsf;
            this.chTsfSuppl = chtsf;
            this.adAddSuppl = adadd;
            this.chAddSuppl = chadd;
            this.CustomerId = customerid;
            this.PriceZone = pricezone;
            this.RateCode = ratecode;
            this.adRate = adrate;
            this.chRate = chrate;
        }
    }
}
