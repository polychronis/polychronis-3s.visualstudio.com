﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;


namespace sss.Tools
{
    class wtCommunication
    {
        static IniFile inifile = new IniFile("c:\\3S Tours\\tours.ini");

        private static string AdaptString(string s)
        {
            //Removes reserved characters from URL string or HTTP or XML Favorite Descriptionor file
            s = s.Replace("&", "&amp;");
            s = s.Replace("'", "&apos;");
            return s;
        }

        public static XmlDocument XMLonHTTP(string input)
        {
            System.Net.WebRequest Request = null;
            XmlDocument doc = new XmlDocument();
            string serverip;

            try
            {
                string currectDir = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                string cipher = inifile.IniReadValue("server", "ip");
                serverip = inifile.Decrypt(cipher, false);
            }
            catch (Exception ex)
            {
                SimpleMessageBox msg = new SimpleMessageBox("Error", "An error has occured. The license file is possibly missing or corrupt. Please contact support with the following message:", ex.Message);
                Nullable<bool> dialogResult = msg.ShowDialog();

                doc.LoadXml(AdaptString("<?xml version=\"1.0\" ?><response>Error</response>"));
                return doc;
            }

            Request = System.Net.WebRequest.Create("http://" + serverip + "/cgi-bin/abtwsac.exe/SssWebToursRequestHandler");
            Request.Method = "POST";
            Request.Timeout = 20 * 1000;

            try
            {
                System.DateTime start_time = DateTime.Now;
                System.IO.Stream RequestStream = Request.GetRequestStream();
                System.Text.ASCIIEncoding ASCIIEncoding = new System.Text.ASCIIEncoding();


                byte[] PostData = ASCIIEncoding.GetBytes(input);
                RequestStream.Write(PostData, 0, PostData.Length);
                RequestStream.Close();

                System.Net.WebResponse wr = Request.GetResponse();
                System.IO.Stream iostr = wr.GetResponseStream();
                System.IO.StreamReader Reader = new System.IO.StreamReader(iostr, Encoding.GetEncoding(1253));

                string ResultHTML = Reader.ReadToEnd();
                doc.LoadXml(AdaptString(ResultHTML));
                TimeSpan elapsed_time = DateTime.Now.Subtract(start_time);
                return doc;
            }
            catch (Exception ex)
            {
                SimpleMessageBox msg = new SimpleMessageBox("Error", "An error has occured. Please contact support with the following message:", ex.Message);
                Nullable<bool> dialogResult = msg.ShowDialog();

                doc.LoadXml(AdaptString("<?xml version=\"1.0\" ?><response>Error</response>"));
                return doc;
            }
        }

        public static Version GetPublishedVersion()
        {
            XmlDocument xmlDoc = new XmlDocument();
            Assembly asmCurrent = System.Reflection.Assembly.GetExecutingAssembly();
            string executePath = new Uri(asmCurrent.GetName().CodeBase).LocalPath;

            xmlDoc.Load(executePath + ".manifest");
            string retval = string.Empty;
            if (xmlDoc.HasChildNodes)
            {
                retval = xmlDoc.ChildNodes[1].ChildNodes[0].Attributes.GetNamedItem("version").Value.ToString();
            }
            return new Version(retval);
        }
    }
}
