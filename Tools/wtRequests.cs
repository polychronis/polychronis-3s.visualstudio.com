﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using sss.Models;


namespace sss.Tools
{
    class wtRequestes
    {
        public static string UserInquiry(string user, string password)
        {
            string request;
            request = "<?xml version=\"1.0\" ?>";
            request += "<aRequest request=\"WEBOTR USER INQUIRY\">";
            request += "<season>" + Convert.ToString(DateTime.Now.Year) + "</season>";
            request += "<user>" + user + "</user>";
            request += "<id>" + password + "</id>";
            request += "</aRequest>";
            return request;
        }

        public static string AvailabilityList2(wtUser user, wtAccommodation acc, DateTime date)
        {
            string request;
            request = "<?xml version=\"1.0\" ?>";
            request += "<aRequest request=\"WEBOTR AVAILABILITY LIST2\">";
            request += "<season>" + user.Season + "</season>";
            request += "<company>" + user.Company + "</company>";
            request += "<user>" + user.Id + "</user>";
            request += "<accommodation>" + acc.Id + "</accommodation>";
            request += "<tourDate>" + string.Format("{0:d/M/yyyy}", date) + "</tourDate>";
            request += "</aRequest>";
            return request;
        }

        public static string AvailabilityList(wtUser user, DateTime date)
        {
            string request = "<?xml version=\"1.0\" ?>";
            request += "<aRequest request=\"WEBOTR AVAILABILITY LIST\">";
            request += "<season>" + user.Season + "</season>";
            request += "<tourDate>" + date.ToString("d/M/yyyy") + "</tourDate>";
            request += "</aRequest>";
            return request;
        }

    }
}
