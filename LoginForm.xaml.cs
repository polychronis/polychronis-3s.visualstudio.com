﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Xml;
using System.Collections.ObjectModel;
using sss.ViewModels;
using sss.Tools;

namespace sss
{
    /// <summary>
    /// Interaction logic for LoginForm.xaml
    /// </summary>
    public partial class LoginForm
    {

        public XmlDocument xmlDoc { get; set; }

        public LoginForm(string title)
        {
            InitializeComponent();
            this.Title = title;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            xmlDoc = wtCommunication.XMLonHTTP(wtRequestes.UserInquiry(tb_username.Text, tb_password.Password));
            this.DialogResult = true;
            this.Close();
        }
    }
}
