﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Xml;
using System.Collections.ObjectModel;
using sss.ViewModels;
using sss.Tools;




namespace sss
{
    /// <summary>
    /// Interaction logic for FarmAboutBox.xaml
    /// </summary>
    public partial class FarmAboutBox
    {
        public FarmAboutBox()
        {
            InitializeComponent();

            lbl_Title.Text = "3S Tours";
            lbl_Subtitle.Text = "Copyright " + DateTime.Now.Year.ToString();

            Version v = wtCommunication.GetPublishedVersion();
            lbl_Version.Text = "V" + v.Major.ToString() + "." + v.Minor.ToString() ;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }
    }
}
