﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Data.SqlClient;

namespace sss
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        public App()
            : base()
        {
            this.Dispatcher.UnhandledException += OnDispatcherUnhandledException;
        }

        void OnDispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {



            try
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString =
                              "Data Source=mssql23.grserver.gr,1444;" +
                              "Initial Catalog=phoenix_desktoplog;" +
                              "User id=desktoplog;" +
                              "Password=!d3skt0pl0g!";

                string myInsertQuery = "INSERT INTO errorlog (client, message, eventtime) Values('xxxx', '" + e.Exception.StackTrace + "', '" + DateTime.Now.ToString() +")";
                SqlCommand myCommand = new SqlCommand(myInsertQuery);
                myCommand.Connection = conn;
                conn.Open();
                myCommand.ExecuteNonQuery();
                myCommand.Connection.Close();
                conn.Close();

            }catch(Exception ex){}



            string errorMessage = string.Format("An unhandled exception occurred: {0}", e.Exception.Message);
            MessageBox.Show(errorMessage, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            e.Handled = true;
        }
    }


}
