﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using sss.Models;
using System.Collections.ObjectModel;
using System.Xml;
using System.Globalization;
using System.Windows.Input;
using sss.Commands;
using System.Windows;
using System.Diagnostics;
using System.ComponentModel;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.Drawing.Printing;
using sss.Tools;
using System.Runtime.CompilerServices;
using System.IO;

namespace sss.ViewModels
{
    class MainWindowViewModel : ViewModel
    {
        #region Item Sources

        public wtUser User { get; set; }

        public ObservableCollection<wtSeller> AvailableSellers { get; set; }
        public ObservableCollection<wtCustomer> AvailableCustomers { get; set; }
        public ObservableCollection<wtMarket> AvailableMarkets { get; set; }
        public ObservableCollection<wtTour> AvailableTours { get; set; }
        public ObservableCollection<wtTktSeries> AvailableTktSeries { get; set; }

        public ObservableCollection<wtMarket> allMarkets { get; set; }
        public ObservableCollection<wtLanguage> allLanguages { get; set; }
        public ObservableCollection<wtAccommodation> allAccommodations { get; set; }
        public ObservableCollection<wtPuPoint> allPuPoints { get; set; }
        public ObservableCollection<wtTour> allTours { get; set; }
        public ObservableCollection<wtBooking> SearchResults { get; set; }

        public ObservableCollection<wtAvailability> AvailabilityResults { get; set; }
        public ObservableCollection<wtSchedule> ScheduleResults { get; set; }

        public ObservableCollection<string> AvailableMailSubjects { get; set; }
        public ObservableCollection<string> AvailableSearchDateBasis { get; set; }



        public wtSearchFields SearchFields { get; set; }

        static IniFile inifile = new IniFile("c:\\3S Tours\\tours.ini");


        #endregion

        #region Commands
        public DelegateCommand DisplayAvailabilityNextCommand { get; private set; }
        public DelegateCommand DisplayAvailabilityPreviousCommand { get; private set; }
        public DelegateCommand DisplayAvailabilityRefreshCommand { get; private set; }
        public DelegateCommand DisplaySearchResultsCommand { get; private set; }
        public DelegateCommand SendEmailCommand { get; private set; }
        public DelegateCommand MakeBookingCommand { get; private set; }
        public DelegateCommand SendEmailNotificationCommand { get; private set; }
        public DelegateCommand ShowInfoCommand { get; private set; }

        public void ShowInfo()
        {
            FarmAboutBox msg = new FarmAboutBox();
            Nullable<bool> dialogResult = msg.ShowDialog();
        }

        public bool CanShowInfo()
        {
            return true;
        }

        public void DisplayAvailabilityNext()
        {
            availabilityDate = availabilityDate.AddDays(7);
            GetAvailabilityResults2(availabilityDate);
            this.NotifyPropertyChanged("AvailabilityDate");
            this.NotifyPropertyChanged("AvailabilityRequestTime");
        }

        public bool CanDisplayAvailabilityNext()
        {
            return true;
        }

        public void DisplayAvailabilityPrevious()
        {
            availabilityDate = availabilityDate.AddDays(-7);
            GetAvailabilityResults2(availabilityDate);
            this.NotifyPropertyChanged("AvailabilityDate");
            this.NotifyPropertyChanged("AvailabilityRequestTime");
        }

        public bool CanDisplayAvailabilityPrevious()
        {
            return true;
        }

        public void DisplayAvailabilityRefresh()
        {
            GetAvailabilityResults2(availabilityDate);
            this.NotifyPropertyChanged("AvailabilityDate");
            this.NotifyPropertyChanged("AvailabilityRequestTime");

        }

        public bool CanDisplayAvailabilityRefresh()
        {
            return true;
        }

        public void MakeBooking()
        {
            wtBooking b = FillBooking();
            XmlDocument dc = wtCommunication.XMLonHTTP(b.MakeRequestString());

            string response = dc.SelectNodes("//response").Item(0).InnerText;
            string msg1 = dc.SelectNodes("//msg1").Item(0).InnerText;
            string msg2 = dc.SelectNodes("//msg2").Item(0).InnerText;
            if (response == "Success")
            {

                b.Id = Convert.ToInt32(dc.SelectNodes("//id").Item(0).InnerText);
                b.EntryDate = DateTime.Now.ToString("dd/MM/yyyy");
                b.EntryTime = DateTime.Now.ToString("HH:mm");
                string tkt = dc.SelectNodes("//file").Item(0).InnerText;

                string[] tktparts = tkt.Split(' ');
                b.TktNo = tktparts[1];

                // Notiifications start
                if (this.NotifyClientEmail == true)
                {
                    if (ClientEmail != "")
                    {
                        SendEmailNotification(b);
                    }
                }

                if (this.NotifyClientCellPhone == true)
                {
                    if (ClientCellPhoneCode != "" && ClientCellPhone != "")
                    {
                        SendSMSNotification(b);
                    }
                }
                // Notifications end

                // Printing start
                PrinterSettings ps = new PrinterSettings();
                ps.PrinterName = SelectedPrinter;
                ps.DefaultPageSettings.PaperSize = new PaperSize("Custom", this.SelectedPrinterPaperWidth, this.SelectedPrinterPaperHeight);
                PrintMessageBox msg = new PrintMessageBox(response, msg1, msg2, b, ps);
                Nullable<bool> dialogResult = msg.ShowDialog();
                // Printing eng

                // Reset form start
                this.SelectedAccommodation = allAccommodations[0];
                this.SelectedPuPoint = allPuPoints[0];
                this.SelectedTktSeries = AvailableTktSeries[0];
                this.SelectedTktNumber = "";
                this.SelectedTour = AvailableTours[0];
                this.SelectedTour_no_stopsales = AvailableTours[0];
                this.ClientName = "";
                this.BookingNotes = "";
                this.NumberOfAdult = "0";
                this.NumberOfChild = "0";
                this.RoomNumber = "";
                this.ClientCellPhone = "";
                this.ClientEmail = "";
                this.NotifyClientCellPhone = false;
                this.NotifyClientEmail = false;
                // Reset form end
            }
            else
            {
                SimpleMessageBox msg = new SimpleMessageBox(response, msg1, msg2);
                Nullable<bool> dialogResult = msg.ShowDialog();
            }
        }

        public bool CanMakeBooking()
        {
            if (this.SelectedLanguage == null) return false;

            if (this.SelectedTime == "") return false;

            if (this.SelectedSeller != null)
            {
                if (this.SelectedSeller.Id == 0) return false;
            }
            else { return false; }

            if (this.SelectedCustomer != null)
            {
                if (this.SelectedCustomer.Id == 0) return false;
            }
            else { return false; }

            if (this.SelectedMarket != null)
            {
                if (this.SelectedMarket.Id == 0) return false;
            }
            else { return false; }

            if (this.SelectedAccommodation != null)
            {
                if (this.SelectedAccommodation.Id == 0) return false;
            }
            else { return false; }

            if (this.SelectedPuPoint != null)
            {
                if (this.SelectedPuPoint.Id == 0) return false;
            }
            else { return false; }

            if (this.SelectedTour != null)
            {
                if (this.SelectedTour.Id == 0) return false;
            }
            else { return false; }


            if (this.ClientName == null || this.ClientName == "") return false;

            //change to Int32.TryParse(
            int a;
            int c;

            if (int.TryParse(this.NumberOfAdult, out a) == false)
            {
                return false;
            }
            else
            {
                if (int.TryParse(this.NumberOfChild, out c) == false)
                {
                    return false;
                }
                else
                {
                    if (a + c == 0) return false;
                    if (a < 0 || c < 0) return false;
                }
            }

            return true;


        }

        public void DisplaySearchResults()
        {
            SearchResults.Clear();

            wtSearchFields sf = new wtSearchFields();
            sf.Company = this.User.Company;
            sf.Season = this.User.Season;
            sf.DateFrom = SelectedSearchDateFrom;
            sf.DateTo = SelectedSearchDateTo;
            sf.DateBasis = SelectedDateBasis + 1;
            sf.Seller = this.SelectedSeller.Id;
            sf.Customer = this.SelectedCustomer.Id;
            //if (this.SelectedTour.Id = null) { sf.Tour = 0; } else { sf.Tour = this.SelectedTour.Id; }
            sf.Tour = 0;
            sf.Point = this.SelectedPuPoint.Id;
            sf.Accommodation = this.SelectedAccommodation.Id;
            sf.Client = this.SearchClientName;

            foreach (wtBooking b in GetSearchResults(sf))
            {
                SearchResults.Add(b);
            }
        }

        public bool CanDisplaySearchResults()
        {


            if (selectedSearchDateFrom <= selectedSearchDateTo)
            {
                if (selectedSearchDateFrom > DateTime.Now && this.SelectedDateBasis == 1) { return false; }

                return true;
            }
            else { return false; }
        }

        public void SendEmailNotification(wtBooking b)
        {
            string LF = Environment.NewLine;

            //(1) Create the MailMessage instance
            MailMessage mm = new MailMessage("mailer@3strading.gr", ClientEmail);

            //(2) Assign the MailMessage's properties
            mm.Subject = "Your tickets to " + b.Tour.Name + " from " + ConfigurationManager.AppSettings.Get("ClientName");

            mm.BodyEncoding = System.Text.Encoding.Unicode;

            mm.Body = "Dear customer, " + LF + "Find attached your ticket information."
                    + LF + LF + "Ticket Series: " + b.TktSeries + "-" + b.TktNo
                    + LF + "Booked on: " + b.EntryDate + " (" + b.EntryTime + ")" + LF
                    + "Tour Name: " + b.Tour.Name + LF + "Tour Date: " + b.TourDate + LF
                    + "Pick Up: " + b.PuTime + " " + b.PuPoint.Name + LF + "Accommodation: " + b.Accommodation.Name;

            if (!string.IsNullOrEmpty(b.Room))
            {
                mm.Body = mm.Body + " (Room " + b.Room + ")" + LF;
            }
            else
            {
                mm.Body = mm.Body + LF;
            }

            mm.Body = mm.Body + "Client Name: " + b.Client + LF;

            if (b.Ad > 0)
            {
                string adultstring = b.Ad.ToString() + " x " + b.AdRate.ToString() + " = " + b.AdAmount.ToString();
                if (b.Ad == 1)
                {
                    mm.Body = mm.Body + LF + "Adult : " + adultstring;
                }
                else
                {
                    mm.Body = mm.Body + LF + "Adults : " + adultstring;
                }
            }

            if (b.Ch > 0)
            {
                string childstring = b.Ch.ToString() + " x " + b.ChRate.ToString() + " = " + b.ChAmount.ToString();
                if (b.Ch == 1)
                {
                    mm.Body = mm.Body + LF + "Child : " + childstring;
                }
                else
                {
                    mm.Body = mm.Body + LF + "Children : " + childstring;
                }
            }

            if (b.Notes != null)
            {
                if (b.Notes.Length > 0)
                {
                    mm.Body = mm.Body + LF + "Notes : " + b.Notes;
                }
            }

            mm.Body = mm.Body + LF + "Total : " + b.Amount;

            mm.Body = mm.Body + LF + LF + LF + "Have a great tour," + LF + this.User.CompanyName + LF + (this.User.CompanyInfo).Replace("\\n", LF) + LF;


            try
            {
                StreamReader fp = new StreamReader("c:\\3S Tours\\footer_client.txt", System.Text.Encoding.Unicode);
                mm.Body = mm.Body + LF + LF + LF + fp.ReadToEnd();

            }
            catch (Exception ex)
            {
                SimpleMessageBox msg = new SimpleMessageBox("Error", "There was an error while reading the footer file. Please contact support with the following message", ex.Message);
                Nullable<bool> dialogResult = msg.ShowDialog();
            }

            mm.IsBodyHtml = false;

            //(3) Create the SmtpClient object
            SmtpClient smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("mailer@3strading.gr", "95149511")
            };


            //(4) Send the MailMessage (will use the Web.config settings)
            try
            {
                smtp.Send(mm);
            }
            catch (Exception ex)
            {
                SimpleMessageBox msg = new SimpleMessageBox("Error", "There was an error while sending the email notification. Please contact support with the following message", ex.Message);
                Nullable<bool> dialogResult = msg.ShowDialog();
            }
        }

        public void SendSMSNotification(wtBooking b)
        {
            string username = inifile.Decrypt(inifile.IniReadValue("sms", "user"), false);
            string password = inifile.Decrypt(inifile.IniReadValue("sms", "password"), false);
            string message = "";

            message = this.User.CompanyName + " Ticket " + b.TktSeries + "-" + b.TktNo + " for " + b.Tour.Name + " Pickup: " + b.TourDate + " - " + b.PuTime;
            if (b.Ad + b.Ch > 1)
            {
                message = message + " Passengers:";
            }
            else
            {
                message = message + " Passenger:";
            }

            if (b.Ad > 0)
            {
                if (b.Ad == 1)
                {
                    message = message + " 1 Adult ";
                }
                else
                {
                    message = message + b.Ad + " Adults ";
                }
            }

            if (b.Ch > 0)
            {
                if (b.Ch == 1)
                {
                    message = message + " 1 Child";
                }
                else
                {
                    message = message + b.Ch + " Children";
                }
            }


            message = Uri.EscapeUriString(message);


            string postString = string.Format("?username={0}&password={1}&to={2}&from={3}&message={4}", username, password, ClientCellPhoneCode + ClientCellPhone, ConfigurationManager.AppSettings.Get("ClientName"), message);

            string URLAuth = "http://www.mysms.com.gr/api.php" + postString;

            const string contentType = "application/x-www-form-urlencoded";
            System.Net.ServicePointManager.Expect100Continue = false;

            CookieContainer cookies = new CookieContainer();
            HttpWebRequest webRequest__1 = WebRequest.Create(URLAuth) as HttpWebRequest;
            webRequest__1.Method = "POST";
            webRequest__1.ContentType = contentType;
            webRequest__1.CookieContainer = cookies;
            webRequest__1.ContentLength = postString.Length;
            webRequest__1.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1";
            webRequest__1.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            webRequest__1.Referer = "http://www.mysms.com.gr/api.php";


            try
            {
                StreamWriter requestWriter = new StreamWriter(webRequest__1.GetRequestStream());
                requestWriter.Write(postString);
                requestWriter.Close();

                StreamReader responseReader = new StreamReader(webRequest__1.GetResponse().GetResponseStream());
                string responseData = responseReader.ReadToEnd();

                responseReader.Close();
                webRequest__1.GetResponse().Close();

            }
            catch (Exception ex)
            {
                SimpleMessageBox msg = new SimpleMessageBox("Error", "There was an error while sending the SMS notification. Please contact support with the following message", ex.Message);
                Nullable<bool> dialogResult = msg.ShowDialog();
            }


        }

        public void SendEmail()
        {
            string LF = Environment.NewLine;

            string recipient = inifile.Decrypt(inifile.IniReadValue("email", "recipient"), false);
            //(1) Create the MailMessage instance
            MailMessage mm = new MailMessage("mailer@3strading.gr", recipient); //local email, client email

            //(2) Assign the MailMessage's properties
            mm.Subject = "subject" + " from " + this.SelectedSeller.Name;

            if (!string.IsNullOrEmpty(this.selectedContactTicketReference))
            {
                mm.Subject = mm.Subject + " about " + this.selectedContactTicketReference;
            }

            mm.Body = "Message from" + LF + "Seller: " + this.SelectedSeller.Name + LF
                + "Agent: " + this.selectedCustomer.Name + LF + "Reply address: "
                + this.selectedContactEmail + LF + "Subject : " + this.selectedMailSubject;

            if (!string.IsNullOrEmpty(this.selectedContactTicketReference))
            {
                mm.Body = mm.Body + " about " + this.selectedContactTicketReference;
            }

            mm.Body = mm.Body + LF + "Request : " + this.emailMessage;

            mm.IsBodyHtml = false;

            //(3) Create the SmtpClient object
            SmtpClient smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("mailer@3strading.gr", "95149511")
            };

            //(4) Send the MailMessage (will use the Web.config settings)
            try
            {
                smtp.Send(mm);
                SimpleMessageBox msg = new SimpleMessageBox("Success", "You message was sent", "");
                Nullable<bool> dialogResult = msg.ShowDialog();
                emailMessage = "";
            }
            catch (Exception ex)
            {
                SimpleMessageBox msg = new SimpleMessageBox("Error", "An error has occured. Please contact support with the following message:", ex.Message);
                Nullable<bool> dialogResult = msg.ShowDialog();
            }

        }

        public bool CanSendEmail()
        {
            return true;
        }

        public string ClientEmail
        {
            get { return clientEmail; }
            set
            {
                clientEmail = value;
                this.NotifyPropertyChanged("ClientEmail");
            }
        }

        public string ClientCellPhoneCode
        {
            get { return clientCellPhoneCode; }
            set
            {
                clientCellPhoneCode = value;
                this.NotifyPropertyChanged("ClientCellPhoneCode");
            }
        }

        public string ClientCellPhone
        {
            get { return clientCellPhone; }
            set
            {
                clientCellPhone = value;
                this.NotifyPropertyChanged("ClientCellPhone");
            }
        }

        public bool HasSMSCredentials
        {
            get
            {
                string username = inifile.Decrypt(inifile.IniReadValue("sms", "user"), false);
                string password = inifile.Decrypt(inifile.IniReadValue("sms", "password"), false);

                if (username != "" && password != "")
                { hasSMSCredentials = true; }
                else { hasSMSCredentials = false; }
                return hasSMSCredentials;
            }
        }

        public bool NotifyClientEmail
        {
            get { return this.notifyClientEmail; }
            set
            {
                this.notifyClientEmail = value;
                this.NotifyPropertyChanged("NotifyClientEmail");
            }
        }

        public bool NotifyClientCellPhone
        {
            get { return this.notifyClientCellPhone; }
            set
            {
                this.notifyClientCellPhone = value;
                this.NotifyPropertyChanged("NotifyClientCellPhone");
            }
        }
        #endregion


        #region Binding

        // Property backing fields.
        wtSeller selectedSeller;
        wtCustomer selectedCustomer;
        wtMarket selectedMarket;
        wtLanguage selectedLanguage;
        wtAccommodation selectedAccommodation;
        wtPuPoint selectedPuPoint;
        wtTour selectedTour;
        wtTour selecterTour_no_stopsales;
        wtTktSeries selectedTktSeries;
        wtBooking selectedSearchResult;


        string clientName;
        string clientCellPhone;
        string clientCellPhoneCode = "30";
        string clientEmail;
        bool notifyClientCellPhone;
        bool notifyClientEmail;
        bool hasSMSCredentials;

        string roomNumber;
        string bookingNotes;
        string selectedTktNumber;
        string selectedTime;


        string numberofAdult = "0";
        string numberofChild = "0";

        string adultPricePerPerson;
        string childPricePerPerson;

        string adultSubTotal;
        string childSubTotal;
        string total;

        string searchClientName;
        int selectedDateBasis = 0;

        //Contact form properties
        string selectedContactEmail;
        string selectedContactTicketReference;
        string emailMessage;
        string selectedMailSubject;

        string selectedPrinter;
        int selectedPrinterPaperWidth;
        int selectedPrinterPaperHeight;
        int selectedPrinterFontSize;
        int selectedPrinterMargins;

        DateTime availabilityDate = DateTime.Now;

        DateTime selectedDate = DateTime.Now;
        DateTime selectedSearchDateFrom = DateTime.Now;
        DateTime selectedSearchDateTo = DateTime.Now;

        CultureInfo culture = CultureInfo.CreateSpecificCulture("el-GR");


        /// <summary>
        /// Gets or sets the Seller.
        /// </summary>
        public wtSeller SelectedSeller
        {
            get { return this.selectedSeller; }
            set
            {
                this.selectedSeller = value;

                //AvailableTktSeries;

                AvailableCustomers = new ObservableCollection<wtCustomer>();
                AvailableMarkets = new ObservableCollection<wtMarket>();
                //TempAvailableTktSeries = new ObservableCollection<wtTktSeries>();

                this.SelectedTime = "";

                if (value != null)
                {
                    //clear and add customers
                    if (AvailableCustomers.Count > 0)
                    {
                        for (int i = AvailableCustomers.Count - 1; i >= 0; i--)
                        {
                            AvailableCustomers.RemoveAt(i);
                        }
                    }
                    foreach (wtCustomer c in this.SelectedSeller.Customers)
                    {
                        AvailableCustomers.Add(c);
                    }
                    if (AvailableCustomers.Count > 1)
                    {
                        AvailableCustomers.Insert(0, new wtCustomer(0, "Select an Agent...", null, null));
                    }
                    this.SelectedCustomer = AvailableCustomers[0];

                    //clear and add tktseries
                    if (AvailableTktSeries.Count > 0)
                    {
                        for (int i = AvailableTktSeries.Count - 1; i >= 0; i--)
                        {
                            AvailableTktSeries.RemoveAt(i);
                        }
                    }
                    foreach (wtTktSeries t in this.selectedSeller.TktSeries)
                    {
                        AvailableTktSeries.Add(t);
                    }
                    this.SelectedTktSeries = AvailableTktSeries[0];

                }


                //this.NotifyPropertyChanged("SelectedSeller");
                //this.NotifyPropertyChanged("SelectedCustomers");
                //this.NotifyPropertyChanged("SelectedTktSeries");
                //this.NotifyPropertyChanged("AllowTktSeriesSelection");
            }
        }


        /// <summary>
        /// Gets or sets the Customer.
        /// </summary>
        public wtCustomer SelectedCustomer
        {
            get { return this.selectedCustomer; }
            set
            {
                this.selectedCustomer = value;
                this.AvailableMarkets.Clear();
                this.SelectedTime = "";

                if (value != null)
                {
                    if (this.selectedCustomer.Market != null)
                    {
                        AvailableMarkets.Add(this.selectedCustomer.Market);
                        //this.selectedMarket = this.selectedCustomer.Market;

                        SelectedMarket = this.selectedCustomer.Market;

                    }
                    else
                    {
                        if (allMarkets != null)
                        {
                            foreach (wtMarket m in allMarkets)
                            {
                                AvailableMarkets.Add(m);
                            }
                        }
                    }

                    if (this.selectedCustomer.Language != null)
                    {
                        SelectedLanguage = this.selectedCustomer.Language;
                    }


                    if (AvailableMarkets.Count > 1)
                    {
                        AvailableMarkets.Insert(0, new wtMarket(0, "Select a Market..."));
                    }
                }

                SelectedContactEmail = this.User.uMail;

                this.NotifyPropertyChanged("AllowMarketSelection");
                this.NotifyPropertyChanged("AllowLanguageSelection");
                this.NotifyPropertyChanged("SelectedMarket");
            }
        }

        /// <summary>
        /// Gets or sets the Accommodation.
        /// </summary>
        public wtAccommodation SelectedAccommodation
        {
            get { return this.selectedAccommodation; }
            set
            {
                if (this.selectedAccommodation == value) return; //put this everywhere!!!
                this.selectedAccommodation = value;
                this.selectedPuPoint = this.selectedAccommodation.PuPoint;

                AvailabilityResults.Clear();
                this.SelectedTime = "";
                this.NotifyPropertyChanged("SelectedAccommodation");
                this.NotifyPropertyChanged("SelectedPuPoint");

                this.NotifyPropertyChanged("SelectedTime");
                this.NotifyPropertyChanged("AdultPricePerPerson");
                this.NotifyPropertyChanged("ChildPricePerPerson");
                this.NotifyPropertyChanged("AdultSubTotal");
                this.NotifyPropertyChanged("ChildSubTotal");
                this.NotifyPropertyChanged("Total");
            }
        }

        /// <summary>
        /// Gets the default pick up point
        /// </summary>
        public wtPuPoint SelectedPuPoint
        {
            get
            {
                if (this.selectedAccommodation == null)
                {
                    return this.allPuPoints[0];
                }

                if (this.selectedAccommodation == allAccommodations[0])
                {
                    return this.allPuPoints[0];
                }
                //return this.selectedAccommodation.PuPoint;
                return this.selectedPuPoint;
            }
            set
            {
                this.SelectedTime = "";
                // if (this.selectedPuPoint == value) return;
                this.selectedPuPoint = value;

                this.NotifyPropertyChanged("SelectedPuPoint");
                this.NotifyPropertyChanged("SelectedTime");
                this.NotifyPropertyChanged("AdultPricePerPerson");
                this.NotifyPropertyChanged("ChildPricePerPerson");
                this.NotifyPropertyChanged("AdultSubTotal");
                this.NotifyPropertyChanged("ChildSubTotal");
                this.NotifyPropertyChanged("Total");
            }
        }


        /// <summary>
        /// Gets the Selected Search Result
        /// </summary>
        public int SelectedDateBasis
        {
            get
            {
                return selectedDateBasis;
            }
            set
            {
                this.selectedDateBasis = value;
            }
        }

        /// <summary>
        /// Gets the Selected Search Result
        /// </summary>
        public wtBooking SelectedSearchResult
        {
            get
            {
                return selectedSearchResult;
            }
            set
            {
                this.selectedSearchResult = value;
            }
        }



        /// <summary>
        /// Gets the Tour
        /// </summary>
        public wtTour SelectedTour_no_stopsales
        {
            get
            {
                //getprices();
                return selectedTour;
            }
            set
            {
                this.selectedTour = value;

                this.NotifyPropertyChanged("SelectedTour");
                this.NotifyPropertyChanged("SelectedTime");
                this.NotifyPropertyChanged("AdultPricePerPerson");
                this.NotifyPropertyChanged("ChildPricePerPerson");
                this.NotifyPropertyChanged("AdultSubTotal");
                this.NotifyPropertyChanged("ChildSubTotal");
                this.NotifyPropertyChanged("Total");
            }
        }

        /// <summary>
        /// Gets the Tour
        /// </summary>
        public wtTour SelectedTour
        {
            get
            {
                //getprices();
                return selectedTour;
            }
            set
            {
                this.selectedTour = value;

                CheckStopSales();

                this.NotifyPropertyChanged("SelectedTour");
                this.NotifyPropertyChanged("SelectedTime");
                this.NotifyPropertyChanged("AdultPricePerPerson");
                this.NotifyPropertyChanged("ChildPricePerPerson");
                this.NotifyPropertyChanged("AdultSubTotal");
                this.NotifyPropertyChanged("ChildSubTotal");
                this.NotifyPropertyChanged("Total");
                this.NotifyPropertyChanged("AllowOverride");

            }
        }

        /// <summary>
        /// Gets or sets the Language.
        /// </summary>
        public wtLanguage SelectedLanguage
        {
            get { return this.selectedLanguage; }
            set
            {
                this.selectedLanguage = value;
                this.NotifyPropertyChanged("SelectedLanguage");
            }
        }


        /// <summary>
        /// Gets Available Tours
        /// </summary>
        private void GetAvailableTours()
        {
            this.AvailableTours.Clear();

            foreach (wtTour t in allTours)
            {
                List<wtTour> provisionalTours = new List<wtTour>();
                List<wtTourSchedule> provisionalSchedules = new List<wtTourSchedule>();
                bool hasmarket = false;

                foreach (wtTourSchedule s in t.Schedules)
                {

                    if (DateTime.Parse(s.DtFrom, culture) <= selectedDate
                        && DateTime.Parse(s.DtTo, culture) >= selectedDate
                        && s.D[(int)selectedDate.DayOfWeek + 1] == 1)
                    {
                        if (s.Market != null) { hasmarket = true; }
                        provisionalSchedules.Add(s);
                    }

                }

                if (AvailableMarkets != null)
                {
                    if (hasmarket == true)
                    {
                        foreach (wtTourSchedule s in provisionalSchedules)
                        {
                            if (s.Market == SelectedMarket)
                            {
                                provisionalTours.Add(t);
                            }
                        }
                    }
                    else
                    {
                        foreach (wtTourSchedule s in provisionalSchedules)
                        {
                            if (s.Market == null)
                            {
                                provisionalTours.Add(t);
                            }
                        }
                    }
                }

                if (provisionalTours.Count > 0)
                {
                    AvailableTours.Add(provisionalTours[0]);
                }
            }



            if (AvailableTours.Count > 0) //used to be >1 but gets stuck in a stopsales loop when there is one tour on stopsales.
            {
                AvailableTours.Insert(0, new wtTour(0, "Select a Tour...", "", "", null, null, null, 0));
                SelectedTour = AvailableTours[0];
            }

        }

        /// <summary>
        /// Gets or sets the Market.
        /// </summary>
        public wtMarket SelectedMarket
        {
            get { return this.selectedMarket; }
            set
            {
                this.selectedMarket = value;
                GetAvailableTours();

            }
        }

        /// <summary>
        /// Gets whether or not the user can override data input.
        /// </summary>
        public bool AllowOverride
        {
            get
            {
                if (this.SelectedTour != null)
                {
                    if (this.SelectedTour.Id > 0)
                    {
                        if (this.SelectedTour.Override == 1 || this.SelectedTime == "")
                        { return true; }
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the Client Name
        /// </summary>
        public string ClientName
        {
            get
            {
                return this.clientName;
            }
            set
            {
                if (value.Length <= 30)
                {
                    clientName = value;
                }
                else
                {
                    clientName = value.Substring(0, 30);
                }
                this.NotifyPropertyChanged("ClientName");
            }
        }

        /// <summary>
        /// Gets the Room Number
        /// </summary>
        public string RoomNumber
        {
            get
            {
                return this.roomNumber;
            }
            set
            {
                if (value.Length <= 5)
                {
                    roomNumber = value;
                }
                else
                {
                    roomNumber = value.Substring(0, 5);
                }
                this.NotifyPropertyChanged("RoomNumber");
            }
        }

        /// <summary>
        /// Gets the Notes
        /// </summary>
        public string BookingNotes
        {
            get
            {
                return this.bookingNotes;
            }
            set
            {
                if (value.Length <= 100)
                {
                    bookingNotes = value;
                }
                else
                {
                    bookingNotes = value.Substring(0, 100);
                }
                this.NotifyPropertyChanged("BookingNotes");
            }
        }




        /// <summary>
        /// Gets whether or not the user should be able to select a seller.
        /// </summary>
        public bool AllowSellerSelection
        {
            get { return (AvailableSellers.Count > 1); }
        }

        /// <summary>
        /// Gets whether or not the user should be able to select a customer.
        /// </summary>
        public bool AllowCustomerSelection
        {
            get { return (AvailableCustomers.Count > 1); }
        }

        /// <summary>
        /// Gets whether or not the user should be able to select a market.
        /// </summary>
        public bool AllowMarketSelection
        {
            get { return (AvailableMarkets.Count > 1); }
        }

        /// <summary>
        /// Gets whether or not the user should be able to select a language.
        /// </summary>
        public bool AllowLanguageSelection
        {
            get { return (allLanguages.Count > 1); }
        }

        /// <summary>
        /// Gets whether or not the user should be able to select an accommodation.
        /// </summary>
        public bool AllowAccommodationSelection
        {
            get { return (allAccommodations.Count > 1); }
        }

        /// <summary>
        /// Gets whether or not the user should be able to select a pickuppoint.
        /// </summary>
        public bool AllowPuPointSelection
        {
            get { return (allPuPoints.Count > 1); }
        }

        /// <summary>
        /// Gets whether or not the user should be able to select a pickuppoint.
        /// </summary>
        public bool AllowTktNumberSelection
        {
            get
            {
                if (this.selectedTktSeries != null)
                {
                    return (this.selectedTktSeries.Auto != 1);
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets whether or not the user should be able to select a pickuppoint.
        /// </summary>
        public bool AllowTktSeriesSelection
        {
            get { return (AvailableTktSeries.Count > 1); }
        }

        /// <summary>
        /// Gets the default language
        /// </summary>
        public wtLanguage SelectDefaultLanguage
        {
            get
            {
                if (this.selectedCustomer != null)
                {
                    return this.selectedCustomer.Language;
                }
                else
                {
                    return null;
                }

            }
            set
            {
                this.selectedLanguage = value;
            }
        }



        /// <summary>
        /// Gets the Client Name 
        /// </summary>
        public string SearchClientName
        {
            get
            {
                return this.searchClientName;
            }
            set
            {
                this.searchClientName = value;
            }
        }


        /// <summary>
        /// Gets the Contact Email for the Contact Form
        /// </summary>
        public string SelectedContactEmail
        {
            get
            {
                return this.selectedContactEmail;
            }
            set
            {
                this.selectedContactEmail = value;
            }
        }

        /// <summary>
        /// Gets the Email Subject
        /// </summary>
        public string SelectedMailSubject
        {
            get
            {
                return this.selectedMailSubject;
            }
            set
            {
                this.selectedMailSubject = value;
            }
        }

        /// <summary>
        /// Gets the Contact Email for the Contact Form
        /// </summary>
        public string EmailMessage
        {
            get
            {
                return this.emailMessage;
            }
            set
            {
                this.emailMessage = value;
            }
        }


        /// <summary>
        /// Gets the Contact Email for the Contact Form
        /// </summary>
        public string SelectedContactTicketReference
        {
            get
            {
                return this.selectedContactTicketReference;
            }
            set
            {
                this.selectedContactTicketReference = value;
            }
        }


        //#region SMS Service

        ///// <summary>
        ///// Gets the SMS Service Username
        ///// </summary>
        //public string SelectedSMSServiceUsername
        //{
        //    get
        //    {
        //        return inifile.IniReadValue("sms", "user");
        //    }
        //    set
        //    {
        //        this.selectedSMSServiceUsername = value;
        //        inifile.IniWriteValue("sms", "user", " " + value);
        //    }
        //}

        ///// <summary>
        ///// Gets the SMS Service Password
        ///// </summary>
        //public string SelectedSMSServicePassword
        //{
        //    get
        //    {
        //        return inifile.IniReadValue("sms", "password");
        //    }
        //    set
        //    {
        //        this.selectedSMSServicePassword = value;
        //        inifile.IniWriteValue("sms", "password", " " + value);
        //    }
        //}


        //#endregion



        #region Printing

        /// <summary>
        /// Gets the Installed Printers
        /// </summary>
        public PrinterSettings.StringCollection InstalledPrinters
        {
            get
            {
                return PrinterSettings.InstalledPrinters;
            }
        }



        /// <summary>
        /// Gets the Selected Printer
        /// </summary>
        public string SelectedPrinter
        {
            get
            {
                string IniPrinter = inifile.IniReadValue("printer", "name");
                if (IniPrinter != "")
                {
                    for (int i = 0; i <= PrinterSettings.InstalledPrinters.Count - 1; i++)
                    {
                        if (IniPrinter.Trim() == PrinterSettings.InstalledPrinters[i])
                        {
                            return IniPrinter;
                        }
                    }
                }
                return this.selectedPrinter;
            }
            set
            {
                this.selectedPrinter = value;
                inifile.IniWriteValue("printer", "name", " " + value);
            }
        }



        /// <summary>
        /// Gets the Printer Paper Size
        /// </summary>
        public int SelectedPrinterPaperWidth
        {
            get
            {
                string IniPrinterPS = inifile.IniReadValue("printer", "paperwidth");
                if (IniPrinterPS != "")
                {
                    return Convert.ToInt16(IniPrinterPS);
                }

                return selectedPrinterPaperWidth;
            }
            set
            {
                this.selectedPrinterPaperWidth = value;
                inifile.IniWriteValue("printer", "paperwidth", " " + value);
            }
        }

        /// <summary>
        /// Gets the Printer Paper Size
        /// </summary>
        public int SelectedPrinterPaperHeight
        {
            get
            {
                string IniPrinterPS = inifile.IniReadValue("printer", "paperheight");
                if (IniPrinterPS != "")
                {
                    return Convert.ToInt16(IniPrinterPS);
                }

                return selectedPrinterPaperHeight;
            }
            set
            {
                this.selectedPrinterPaperHeight = value;
                inifile.IniWriteValue("printer", "paperheight", " " + value);
            }
        }

        /// <summary>
        /// Gets the Printer Font Size
        /// </summary>
        public int SelectedPrinterFontSize
        {
            get
            {
                string IniPrinterFS = inifile.IniReadValue("printer", "fontsize");
                if (IniPrinterFS != "")
                {
                    return Convert.ToInt16(IniPrinterFS);
                }

                return selectedPrinterFontSize;
            }
            set
            {
                this.selectedPrinterFontSize = value;
                inifile.IniWriteValue("printer", "fontsize", " " + value);
            }
        }

        /// <summary>
        /// Gets the Printer Margins
        /// </summary>
        public int SelectedPrinterMargins
        {
            get
            {
                string IniMargins = inifile.IniReadValue("printer", "margins");
                if (IniMargins != "")
                {
                    return Convert.ToInt16(IniMargins);
                }

                return selectedPrinterMargins;
            }
            set
            {
                this.selectedPrinterMargins = value;
                inifile.IniWriteValue("printer", "margins", " " + value);
            }
        }

        #endregion

        private decimal[] getprices()
        {
            decimal adValue = 0;
            decimal chValue = 0;
            decimal[] prices = new decimal[2];

            if (this.SelectedTour != null)
            {
                if (this.SelectedTour.Id > 0 && this.SelectedDate != null && this.SelectedCustomer != null)
                {
                    if (this.SelectedPuPoint != null)
                    {
                        if (this.SelectedPuPoint.Id > 0)
                        {
                            foreach (wtRate r in SelectedTour.Rates)
                            {
                                if (DateTime.Parse(r.DtFrom, culture) <= DateTime.Parse(SelectedDate.ToString(), culture)
                                && DateTime.Parse(r.DtTo, culture) >= DateTime.Parse(SelectedDate.ToString(), culture))
                                {
                                    if (r.CustomerId == this.SelectedCustomer.Id)
                                    {
                                        if (r.PriceZone == SelectedPuPoint.PzId)
                                        {
                                            adValue = r.adTsfSuppl + r.adAddSuppl + r.adRate;
                                            chValue = r.chTsfSuppl + r.chAddSuppl + r.chRate;

                                            prices[0] = adValue;
                                            prices[1] = chValue;
                                            return prices;
                                        }
                                    }
                                }
                            }
                            foreach (wtRate r in SelectedTour.Rates)
                            {
                                if (DateTime.Parse(r.DtFrom, culture) <= DateTime.Parse(SelectedDate.ToString(), culture)
                                && DateTime.Parse(r.DtTo, culture) >= DateTime.Parse(SelectedDate.ToString(), culture))
                                {
                                    if (r.CustomerId == this.SelectedCustomer.Id)
                                    {
                                        if (r.PriceZone == 0)
                                        {
                                            adValue = r.adTsfSuppl + r.adAddSuppl + r.adRate;
                                            chValue = r.chTsfSuppl + r.chAddSuppl + r.chRate;

                                            prices[0] = adValue;
                                            prices[1] = chValue;
                                            return prices;
                                        }
                                    }
                                }
                            }
                            foreach (wtRate r in selectedTour.Rates)
                            {
                                if (DateTime.Parse(r.DtFrom, culture) <= DateTime.Parse(SelectedDate.ToString(), culture)
                                && DateTime.Parse(r.DtTo, culture) >= DateTime.Parse(SelectedDate.ToString(), culture))
                                {
                                    if (r.CustomerId == 0)
                                    {
                                        if (r.PriceZone == SelectedPuPoint.PzId)
                                        {
                                            adValue = r.adTsfSuppl + r.adAddSuppl + r.adRate;
                                            chValue = r.chTsfSuppl + r.chAddSuppl + r.chRate;

                                            prices[0] = adValue;
                                            prices[1] = chValue;
                                            return prices;
                                        }
                                    }
                                }
                            }
                            foreach (wtRate r in selectedTour.Rates)
                            {
                                if (DateTime.Parse(r.DtFrom, culture) <= DateTime.Parse(SelectedDate.ToString(), culture)
                                && DateTime.Parse(r.DtTo, culture) >= DateTime.Parse(SelectedDate.ToString(), culture))
                                {
                                    if (r.CustomerId == 0)
                                    {
                                        if (r.PriceZone == 0)
                                        {
                                            adValue = r.adTsfSuppl + r.adAddSuppl + r.adRate;
                                            chValue = r.chTsfSuppl + r.chAddSuppl + r.chRate;

                                            prices[0] = adValue;
                                            prices[1] = chValue;
                                            return prices;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            prices[0] = adValue;
            prices[1] = chValue;
            return prices;
        }

        /// <summary>
        /// Gets the Number of Adults
        /// </summary>
        public string NumberOfAdult
        {
            get
            {

                return numberofAdult;

            }
            set
            {

                this.numberofAdult = value;



                this.NotifyPropertyChanged("NumberOfAdult");
                this.NotifyPropertyChanged("AdultSubTotal");
                this.NotifyPropertyChanged("Total");


            }
        }

        /// <summary>
        /// Gets the Number of Children
        /// </summary>
        public string NumberOfChild
        {
            get
            {
                return numberofChild;
            }
            set
            {
                this.numberofChild = value;

                this.NotifyPropertyChanged("NumberOfChild");
                this.NotifyPropertyChanged("ChildSubTotal");
                this.NotifyPropertyChanged("Total");
            }
        }

        /// <summary>
        /// Gets the Adult Price Per Person
        /// </summary>
        public string AdultPricePerPerson
        {
            get
            {
                adultPricePerPerson = getprices()[0].ToString();
                return adultPricePerPerson;
            }
            set
            {
                this.adultPricePerPerson = value;
                this.NotifyPropertyChanged("AdultSubTotal");
                this.NotifyPropertyChanged("Total");

            }
        }

        /// <summary>
        /// Gets the Child Price Per Person
        /// </summary>
        public string ChildPricePerPerson
        {
            get
            {

                childPricePerPerson = getprices()[1].ToString();
                return childPricePerPerson;
            }
            set
            {
                this.childPricePerPerson = value;
                this.NotifyPropertyChanged("ChildSubTotal");
                this.NotifyPropertyChanged("Total");


            }
        }


        /// <summary>
        /// Gets the Adult SubTotal
        /// </summary>
        public string AdultSubTotal
        {
            get
            {

                int a;
                decimal b;
                if (int.TryParse(NumberOfAdult, out a) == true)
                {

                    if (a > 0)
                    {
                        if (decimal.TryParse(this.AdultPricePerPerson, out b) == true)
                        {
                            if (b >= 0)
                            {
                                adultSubTotal = Convert.ToString(a * b);
                            }
                            if (b == 0) childSubTotal = "0";
                            if (b < 0) childSubTotal = "Negative";
                        }



                    }
                    if (a == 0) adultSubTotal = "0";
                    if (a < 0) adultSubTotal = "Negative";

                }
                else
                {
                    adultSubTotal = "Invalid";
                }
                return adultSubTotal;
            }
            set
            {
                this.adultSubTotal = value;
            }
        }

        /// <summary>
        /// Gets the Children SubTotal
        /// </summary>
        public string ChildSubTotal
        {
            get
            {
                int a;
                decimal b;

                if (int.TryParse(NumberOfChild, out a) == true)
                {

                    if (a > 0)
                    {

                        if (decimal.TryParse(this.ChildPricePerPerson, out b) == true)
                        {
                            if (b >= 0)
                            {
                                childSubTotal = Convert.ToString(a * b);
                            }
                            if (b == 0) childSubTotal = "0";
                            if (b < 0) childSubTotal = "Negative";
                        }
                    }
                    if (a == 0) childSubTotal = "0";
                    if (a < 0) childSubTotal = "Negative";

                }
                else
                {
                    childSubTotal = "Invalid";
                }
                return childSubTotal;
            }
            set
            {
                this.childSubTotal = value;
            }
        }

        /// <summary>
        /// Gets the Total
        /// </summary>
        public string Total
        {
            get
            {

                decimal a;
                decimal b;
                if (decimal.TryParse(adultSubTotal, out a) == true)
                {
                    if (a >= 0)
                    {
                        if (decimal.TryParse(childSubTotal, out b) == true)
                        {
                            if (b >= 0)
                            {
                                total = (a + b).ToString();
                            }
                            if (b < 0) total = "Negative";
                        }
                    }
                    if (a < 0) total = "Negative";

                }
                else
                {
                    total = "Invalid";
                }
                return total;
            }
            set
            {
                this.total = value;
            }
        }

        /// <summary>
        /// Gets the Selected Search Date To
        /// </summary>
        public DateTime SelectedSearchDateTo
        {
            get
            {
                return selectedSearchDateTo;
            }
            set
            {
                this.selectedSearchDateTo = value;

            }
        }

        /// <summary>
        /// Gets the Selected Search Date From
        /// </summary>
        public DateTime SelectedSearchDateFrom
        {
            get
            {
                return selectedSearchDateFrom;
            }
            set
            {
                this.selectedSearchDateFrom = value;

            }
        }

        /// <summary>
        /// Gets the Date
        /// </summary>
        public DateTime SelectedDate
        {
            get
            {
                return selectedDate;
            }
            set
            {
                this.selectedDate = value;

                GetAvailableTours();

                this.NotifyPropertyChanged("SelectedTour");
                this.NotifyPropertyChanged("SelectedTime");
                this.NotifyPropertyChanged("AdultValue");
                this.NotifyPropertyChanged("ChildValue");

            }
        }

        /// <summary>
        /// Gets the Date
        /// </summary>
        public string AvailabilityDate
        {
            get
            {
                string from = availabilityDate.ToString("dd/MM/yyyy");
                string to = availabilityDate.AddDays(6).ToString("dd/MM/yyyy");
                return "Week " + from + " to " + to;
            }
        }

        /// <summary>
        /// Gets the Date
        /// </summary>
        public string AvailabilityRequestTime
        {
            get
            {

                return "Requested on " + DateTime.Now.ToString("HH:mm dd/MM/yyyy");
            }
        }

        /// <summary>
        /// Gets the Ticket Series
        /// </summary>
        public wtTktSeries SelectedTktSeries
        {
            get
            {
                return selectedTktSeries;
            }
            set
            {
                this.selectedTktSeries = value;

                this.NotifyPropertyChanged("SelectedTktSeries");
                this.NotifyPropertyChanged("SelectedTktNumber");
                this.NotifyPropertyChanged("AllowTktNumberSelection");
            }
        }

        /// <summary>
        /// Gets the Ticket Number
        /// </summary>
        public string SelectedTktNumber
        {
            get
            {
                if (this.selectedTktSeries != null)
                {
                    if (this.selectedTktSeries.Auto == 1)
                    {
                        return "AUTO";
                    }
                    else { return this.selectedTktNumber; }
                }
                else
                {
                    return this.selectedTktNumber;
                }
            }
            set
            {
                this.selectedTktNumber = value;
                //this.NotifyPropertyChanged("SelectedTktNumber");
            }
        }



        /// <summary>
        /// Gets the Time
        /// </summary>
        public string SelectedTime
        {
            get
            {
                if (this.SelectedPuPoint != null && this.SelectedTour != null && this.SelectedDate != null)
                {
                    if (this.SelectedTour.Id > 0)
                    {
                        foreach (wtTourTimeTable t in this.SelectedTour.TimeTables)
                        {
                            if (DateTime.Parse(t.DtFrom, culture) < this.SelectedDate
                                && DateTime.Parse(t.DtTo, culture) > this.SelectedDate)
                                if (t.Market == null)
                                {
                                    foreach (wtTimeTablePUT tt in t.PuTimes)
                                    {
                                        if (tt.PointId == this.SelectedPuPoint.Id)
                                        {
                                            return tt.Time;
                                        }
                                    }
                                }
                                else
                                {
                                    if (t.Market == this.SelectedMarket)
                                    {
                                        foreach (wtTimeTablePUT tt in t.PuTimes)
                                        {
                                            if (tt.PointId == this.SelectedPuPoint.Id)
                                            {
                                                return tt.Time;
                                            }
                                        }
                                    }
                                }
                            {

                            }
                        }
                    }
                }
                return this.selectedTime;
            }
            set
            {
                this.selectedTime = value;
                //this.NotifyPropertyChanged("SelectedTime");
            }
        }

        #endregion

        #region Constructors

        public MainWindowViewModel()
        {
            // Instantiate our item sources.
            this.User = new wtUser();
            this.AvailableSellers = new ObservableCollection<wtSeller>();
            this.AvailableCustomers = new ObservableCollection<wtCustomer>();
            this.AvailableMarkets = new ObservableCollection<wtMarket>();
            this.AvailableTours = new ObservableCollection<wtTour>();
            this.AvailableTktSeries = new ObservableCollection<wtTktSeries>();

            this.allMarkets = new ObservableCollection<wtMarket>();
            this.allLanguages = new ObservableCollection<wtLanguage>();
            this.allAccommodations = new ObservableCollection<wtAccommodation>();
            this.allPuPoints = new ObservableCollection<wtPuPoint>();
            this.allTours = new ObservableCollection<wtTour>();

            this.SearchResults = new ObservableCollection<wtBooking>();
            this.DisplaySearchResultsCommand = new DelegateCommand(this.DisplaySearchResults, CanDisplaySearchResults);
            this.SendEmailCommand = new DelegateCommand(this.SendEmail, CanSendEmail);
            this.MakeBookingCommand = new DelegateCommand(this.MakeBooking, CanMakeBooking);

            this.DisplayAvailabilityNextCommand = new DelegateCommand(this.DisplayAvailabilityNext, this.CanDisplayAvailabilityNext);
            this.DisplayAvailabilityPreviousCommand = new DelegateCommand(this.DisplayAvailabilityPrevious, this.CanDisplayAvailabilityPrevious);
            this.DisplayAvailabilityRefreshCommand = new DelegateCommand(this.DisplayAvailabilityRefresh, this.CanDisplayAvailabilityRefresh);
            this.ShowInfoCommand = new DelegateCommand(this.ShowInfo, this.CanShowInfo);

            this.AvailabilityResults = new ObservableCollection<wtAvailability>();
            this.ScheduleResults = new ObservableCollection<wtSchedule>();

            this.AvailableMailSubjects = new ObservableCollection<string>();
            this.AvailableSearchDateBasis = new ObservableCollection<string>();

            // Simulate data retrieval.
            if (this.User.Id == 0)
            {
                bool designTime = System.ComponentModel.DesignerProperties.GetIsInDesignMode(new DependencyObject());
                if (!designTime)
                {
                    // Do something that only happens on Design mode

                    // Instantiate window
                    Version v = wtCommunication.GetPublishedVersion();
                    LoginForm Loginfrm = new LoginForm("3S Tours Desktop V" + v.Major.ToString() + "." + v.Minor.ToString() + " -  Please Login");

                    // Show window modally 
                    // NOTE: Returns only when window is closed
                    Nullable<bool> dialogResult = Loginfrm.ShowDialog();


                    if (dialogResult == false)
                    {
                        Loginfrm.Close();
                        Environment.Exit(0);
                    }
                    else
                    {
                        while (this.my3Slogin(Loginfrm.xmlDoc) != true)
                        {
                            LoginForm Loginfrm1 = new LoginForm("Please try again");
                            dialogResult = Loginfrm1.ShowDialog();
                            Loginfrm = Loginfrm1;
                            if (dialogResult == false)
                            {
                                Environment.Exit(0);
                            }
                        }

                        //GetAvailabilityResults(DateTime.Now);
                    }
                }
            }




        }

        #endregion




        #region Stop Sales
        protected void CheckStopSales()
        {
            if (SelectedTour != null)
            {
                if (SelectedTour.ssTime != null && SelectedTour.ssTime != "")
                {
                    string str1 = "";
                    string[] ssTimeArray = SelectedTour.ssTime.Split(':');
                    TimeSpan ssTime = new TimeSpan(Convert.ToInt16(ssTimeArray[0]),
                                                       Convert.ToInt16(ssTimeArray[1]),
                                                       Convert.ToInt16(ssTimeArray[2]));
                    //TimeSpan current_time = DateTime.Now.TimeOfDay;
                    //TimeSpan tdiff = ssTime.Subtract(current_time);

                    switch (SelectedTour.ssDay)
                    {
                        case "1":
                            break;
                        case "2":
                            if (this.SelectedDate + ssTime <= DateTime.Now)
                            {
                                str1 = "Sales stopped at " + this.SelectedTour.ssTime.Remove(this.SelectedTour.ssTime.Length - 3, 3);
                                //this.SelectedTour.Id = 0;

                                SimpleMessageBox msg = new SimpleMessageBox("Stop sales detected", str1, "");
                                Nullable<bool> dialogResult = msg.ShowDialog();

                                this.SelectedTour = AvailableTours[0];
                            }
                            break;
                        case "3":
                            if (this.SelectedDate.AddDays(-1) + ssTime <= DateTime.Now)
                            {
                                str1 = "Sales stopped on " + this.SelectedDate.AddDays(-1).ToString("dd/MM/yyyy")
                                                           + " at "
                                                           + this.SelectedTour.ssTime.Remove(this.SelectedTour.ssTime.Length - 3, 3);

                                //this.SelectedTour.Id = 0;


                                SimpleMessageBox msg = new SimpleMessageBox("Stop sales detected", str1, "");
                                Nullable<bool> dialogResult = msg.ShowDialog();

                                this.SelectedTour = AvailableTours[0];
                            }
                            break;
                    }
                }
            }
        }
        #endregion

        #region "3S Authenticate"
        private bool my3Slogin(XmlDocument xmlDoc)
        {

            string response = xmlDoc.SelectNodes("//response").Item(0).InnerText;
            if (response == "Success")
            {

                this.User.Company = xmlDoc.SelectNodes("//company").Item(0).InnerText;
                this.User.Season = Convert.ToString(DateTime.Now.Year);
                this.User.Id = Convert.ToInt32(xmlDoc.SelectNodes("//id").Item(0).InnerText);
                this.User.Name = xmlDoc.SelectNodes("//name").Item(0).InnerText;
                this.User.CompanyName = xmlDoc.SelectNodes("//cName").Item(0).InnerText;
                this.User.CompanyInfo = xmlDoc.SelectNodes("//cInfo").Item(0).InnerText;
                this.User.LoginDT = DateTime.Now.ToShortTimeString();
                this.User.uMail = xmlDoc.SelectNodes("//uMail").Item(0).InnerText;

                loadmarkets(xmlDoc.SelectNodes("//Markets/Item"));
                loadlanguages(xmlDoc.SelectNodes("//Languages/Item"));
                loadsellers(xmlDoc.SelectNodes("//Sellers/Seller"));
                loadPuPoints(xmlDoc.SelectNodes("//PuPoints/Item"));
                loadAccommodations(xmlDoc.SelectNodes("//Accommodations/Accommodation"));
                loadTours(xmlDoc.SelectNodes("//Tours/Tour"), allMarkets);
                loadMailSubjects();
                loadSearchDateBasis();

                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        private wtBooking FillBooking()
        {
            wtBooking b = new wtBooking();
            b.Ad = Convert.ToInt16(this.NumberOfAdult);
            b.AdRate = Convert.ToDecimal(this.AdultPricePerPerson);
            b.Accommodation = this.SelectedAccommodation;
            b.User = this.User;
            b.Customer = this.SelectedCustomer.Id.ToString();
            b.Language = this.SelectedLanguage;
            b.Market = this.SelectedMarket;
            b.Notes = this.BookingNotes;
            b.Ch = Convert.ToInt16(this.NumberOfChild);
            b.Client = this.ClientName;
            b.ChRate = Convert.ToDecimal(this.ChildPricePerPerson);
            b.PuPoint = this.SelectedPuPoint;
            b.Seller = this.SelectedSeller;
            b.Tour = this.SelectedTour;
            b.PuTime = this.SelectedTime;
            b.RateCode = "";
            b.Room = this.RoomNumber;
            b.TourDate = this.SelectedDate.ToString("dd/MM/yyyy");
            b.TktNo = this.SelectedTktNumber;
            b.TktSeries = this.SelectedTktSeries.Code;
            b.Season = this.User.Season;
            b.AdAmount = Convert.ToDecimal(this.AdultSubTotal);
            b.ChAmount = Convert.ToDecimal(this.ChildSubTotal);

            int i = this.AllowOverride ? 1 : 0;

            b.IsOverride = i.ToString();
            b.TotAmount = b.AdAmount + b.ChAmount;


            return b;
        }

        #region Availability Results

        private void GetAvailabilityResults2(DateTime date)
        {

            XmlDocument xmlDoc = wtCommunication.XMLonHTTP(wtRequestes.AvailabilityList2(this.User, SelectedAccommodation, date));

            string response = xmlDoc.SelectNodes("//response").Item(0).InnerText;
            if (response == "Success")
            {
                XmlNode xmldates = xmlDoc.SelectSingleNode("//Dates");

                ScheduleResults.Clear();
                wtSchedule Schedule = new wtSchedule();

                Schedule.Day1 = xmldates.SelectNodes("d1").Item(0).InnerText;
                Schedule.Day2 = xmldates.SelectNodes("d2").Item(0).InnerText;
                Schedule.Day3 = xmldates.SelectNodes("d3").Item(0).InnerText;
                Schedule.Day4 = xmldates.SelectNodes("d4").Item(0).InnerText;
                Schedule.Day5 = xmldates.SelectNodes("d5").Item(0).InnerText;
                Schedule.Day6 = xmldates.SelectNodes("d6").Item(0).InnerText;
                Schedule.Day7 = xmldates.SelectNodes("d7").Item(0).InnerText;

                ScheduleResults.Add(Schedule);
                XmlNodeList xmlavails = xmlDoc.SelectNodes("//Availability/Plan");

                AvailabilityResults.Clear();

                foreach (XmlNode n in xmlavails)
                {

                    wtAvailability av = new wtAvailability(
                        n.SelectNodes("name").Item(0).InnerText,
                        n.SelectNodes("a1").Item(0).InnerText,
                        n.SelectNodes("a2").Item(0).InnerText,
                        n.SelectNodes("a3").Item(0).InnerText,
                        n.SelectNodes("a4").Item(0).InnerText,
                        n.SelectNodes("a5").Item(0).InnerText,
                        n.SelectNodes("a6").Item(0).InnerText,
                        n.SelectNodes("a7").Item(0).InnerText);

                    AvailabilityResults.Add(av);
                }
            }
        }

        private void GetAvailabilityResults(DateTime date)
        {


            XmlDocument xmlDoc = wtCommunication.XMLonHTTP(wtRequestes.AvailabilityList(this.User, date));

            string response = xmlDoc.SelectNodes("//response").Item(0).InnerText;
            if (response == "Success")
            {
                XmlNode xmldates = xmlDoc.SelectSingleNode("//Dates");

                ScheduleResults.Clear();
                wtSchedule Schedule = new wtSchedule();

                Schedule.Day1 = xmldates.SelectNodes("d1").Item(0).InnerText;
                Schedule.Day2 = xmldates.SelectNodes("d2").Item(0).InnerText;
                Schedule.Day3 = xmldates.SelectNodes("d3").Item(0).InnerText;
                Schedule.Day4 = xmldates.SelectNodes("d4").Item(0).InnerText;
                Schedule.Day5 = xmldates.SelectNodes("d5").Item(0).InnerText;
                Schedule.Day6 = xmldates.SelectNodes("d6").Item(0).InnerText;
                Schedule.Day7 = xmldates.SelectNodes("d7").Item(0).InnerText;

                ScheduleResults.Add(Schedule);
                XmlNodeList xmlavails = xmlDoc.SelectNodes("//Availability/Plan");

                AvailabilityResults.Clear();

                foreach (XmlNode n in xmlavails)
                {

                    wtAvailability av = new wtAvailability(
                        n.SelectNodes("name").Item(0).InnerText,
                        n.SelectNodes("a1").Item(0).InnerText,
                        n.SelectNodes("a2").Item(0).InnerText,
                        n.SelectNodes("a3").Item(0).InnerText,
                        n.SelectNodes("a4").Item(0).InnerText,
                        n.SelectNodes("a5").Item(0).InnerText,
                        n.SelectNodes("a6").Item(0).InnerText,
                        n.SelectNodes("a7").Item(0).InnerText);

                    AvailabilityResults.Add(av);
                }
            }
        }

        #endregion

        #region Search Results

        private ObservableCollection<wtBooking> GetSearchResults(wtSearchFields sf)
        {
            CultureInfo culture = new CultureInfo("en-US");

            ObservableCollection<wtBooking> sr = new ObservableCollection<wtBooking>();

            XmlDocument xmlDoc = wtCommunication.XMLonHTTP(sf.MakeRequestString());

            string response = xmlDoc.SelectNodes("//response").Item(0).InnerText;
            if (response == "Success")
            {
                XmlNodeList xmlbookings = xmlDoc.SelectNodes("//Items/Booking");
                foreach (XmlNode n in xmlbookings)
                {
                    wtBooking b = new wtBooking();
                    b.Id = Convert.ToInt16(n.SelectNodes("//id").Item(0).InnerText);

                    wtAccommodation ac = new wtAccommodation();
                    ac.Name = n.SelectNodes("accommodation").Item(0).InnerText;
                    b.Accommodation = ac;
                    b.Ad = Convert.ToInt16(n.SelectNodes("ad").Item(0).InnerText);
                    b.AdAddSuppl = n.SelectNodes("adAddSuppl").Item(0).InnerText;
                    b.AdAmount = Convert.ToDecimal(n.SelectNodes("adAmount").Item(0).InnerText, culture);
                    b.AdFree = n.SelectNodes("adFree").Item(0).InnerText;
                    b.AdPrice = n.SelectNodes("adPrice").Item(0).InnerText;
                    b.AdRate = Convert.ToDecimal(n.SelectNodes("adRate").Item(0).InnerText, culture);
                    b.AdTsfSuppl = n.SelectNodes("adTsfSuppl").Item(0).InnerText;
                    b.AgentName = n.SelectNodes("agentName").Item(0).InnerText;
                    b.Notes = n.SelectNodes("notes").Item(0).InnerText;
                    b.Amount = n.SelectNodes("amount").Item(0).InnerText;
                    b.Ch = Convert.ToInt16(n.SelectNodes("ch").Item(0).InnerText);
                    b.ChAddSuppl = n.SelectNodes("chAddSuppl").Item(0).InnerText;
                    b.ChAmount = Convert.ToDecimal(n.SelectNodes("chAmount").Item(0).InnerText, culture);
                    b.ChFree = n.SelectNodes("chFree").Item(0).InnerText;
                    b.ChPrice = n.SelectNodes("chPrice").Item(0).InnerText;
                    b.ChRate = Convert.ToDecimal(n.SelectNodes("chRate").Item(0).InnerText, culture);
                    b.ChTsfSuppl = n.SelectNodes("chTsfSuppl").Item(0).InnerText;

                    b.Client = n.SelectNodes("client").Item(0).InnerText;
                    b.EntryDate = n.SelectNodes("entryDate").Item(0).InnerText;
                    b.EntryTime = n.SelectNodes("entryTime").Item(0).InnerText;

                    wtLanguage la = new wtLanguage();
                    la.Name = n.SelectNodes("language").Item(0).InnerText;
                    b.Language = la;

                    wtMarket ma = new wtMarket();
                    ma.Name = n.SelectNodes("market").Item(0).InnerText;
                    b.Market = ma;

                    wtPuPoint pu = new wtPuPoint();
                    pu.Name = n.SelectNodes("pointName").Item(0).InnerText;
                    b.PuPoint = pu;

                    b.PuTime = n.SelectNodes("puTime").Item(0).InnerText;
                    b.Room = n.SelectNodes("room").Item(0).InnerText;

                    wtSeller se = new wtSeller();
                    se.Name = n.SelectNodes("sellerName").Item(0).InnerText;
                    b.Seller = se;

                    b.TktNo = n.SelectNodes("tktNo").Item(0).InnerText;
                    b.TktSeries = n.SelectNodes("tktSeries").Item(0).InnerText;
                    b.TourDate = n.SelectNodes("tourDate").Item(0).InnerText;

                    wtTour to = new wtTour();
                    to.Name = n.SelectNodes("tourName").Item(0).InnerText;
                    b.Tour = to;

                    b.UserName = n.SelectNodes("userName").Item(0).InnerText;

                    sr.Add(b);
                }
            }

            return sr;
        }

        #endregion

        #region "LoadSecTables"

        private void loadMailSubjects()
        {
            AvailableMailSubjects.Insert(0, "Select a subject...");
            AvailableMailSubjects.Insert(1, "Notification");
            AvailableMailSubjects.Insert(2, "Modification");
            AvailableMailSubjects.Insert(3, "Cancelation");
        }

        private void loadSearchDateBasis()
        {
            AvailableSearchDateBasis.Insert(0, "Tour Date");
            AvailableSearchDateBasis.Insert(1, "Entry Date");
        }



        private void loadPuPoints(XmlNodeList xmlnode)
        {
            foreach (XmlNode node in xmlnode)
            {
                wtPuPoint p = new wtPuPoint();
                p.Id = Convert.ToInt32(node["id"].InnerText);
                p.Name = node["name"].InnerText;
                p.PzId = Convert.ToInt32(node["priceZone"].InnerText);
                allPuPoints.Add(p);
            }
            if (allPuPoints.Count > 0) //used to be >1 but gets stuck in the time selection.
            {
                allPuPoints.Insert(0, new wtPuPoint(0, "Select a Pickup Point...", 0));
            }
        }

        private void loadAccommodations(XmlNodeList xmlnode)
        {
            foreach (XmlNode node in xmlnode)
            {
                wtAccommodation a = new wtAccommodation();
                a.Id = Convert.ToInt32(node["id"].InnerText);
                a.Name = node["name"].InnerText;

                int pupointid = Convert.ToInt32(node["puPoint"].InnerText);
                a.PuPoint = allPuPoints.Where(x => x.Id == pupointid).FirstOrDefault();
                allAccommodations.Add(a);
            }
            if (allAccommodations.Count > 0) //used to be >1 but gets stuck in the pickup point selection process
            {
                allAccommodations.Insert(0, new wtAccommodation(0, "Select an Accommodation...",
                                  new wtPuPoint(0, "Select a Pickup Point...", 0)));

            }
        }


        private void loadmarkets(XmlNodeList xmlnode)
        {
            foreach (XmlNode node in xmlnode)
            {
                wtMarket m = new wtMarket();
                m.Id = Convert.ToInt32(node["id"].InnerText);
                m.Name = node["name"].InnerText;
                this.allMarkets.Add(m);
            }
        }

        private void loadlanguages(XmlNodeList xmlnode)
        {
            foreach (XmlNode node in xmlnode)
            {
                wtLanguage l = new wtLanguage();
                l.Id = Convert.ToInt32(node["id"].InnerText);
                l.Name = node["name"].InnerText;
                this.allLanguages.Add(l);
            }
        }

        private void loadsellers(XmlNodeList xmlnode)
        {

            foreach (XmlNode node in xmlnode)
            {
                wtSeller s = new wtSeller();
                s.Id = Convert.ToInt32(node["id"].InnerText);
                s.Name = node["name"].InnerText;

                //Customers Load--start
                if (node.InnerXml.Contains("Agents"))
                {

                    List<wtCustomer> cs = new List<wtCustomer>();

                    foreach (XmlNode n in node["Agents"].ChildNodes)
                    {
                        wtCustomer c = new wtCustomer();
                        c.Id = Convert.ToInt32(n["id"].InnerText);
                        c.Name = n["name"].InnerText;

                        int languageid = Convert.ToInt32(n["language"].InnerText);
                        c.Language = allLanguages.Where(x => x.Id == languageid).FirstOrDefault();

                        int marketid = Convert.ToInt32(n["market"].InnerText);
                        c.Market = allMarkets.Where(x => x.Id == marketid).FirstOrDefault();

                        cs.Add(c);
                    }

                    s.Customers = cs;
                }
                //Tktseries Load
                if (node.InnerXml.Contains("TktSeries"))
                {

                    List<wtTktSeries> ts = new List<wtTktSeries>();

                    foreach (XmlNode n in node["TktSeries"].ChildNodes)
                    {
                        wtTktSeries t = new wtTktSeries();
                        t.Code = n["code"].InnerText;
                        t.Auto = Convert.ToInt32(n["auto"].InnerText);

                        ts.Add(t);
                    }

                    s.TktSeries = ts;
                }

                this.AvailableSellers.Add(s);
                this.NotifyPropertyChanged("AllowSellerSelection");
            }
        }

        private void loadTours(XmlNodeList xmlnode, ObservableCollection<wtMarket> markets)
        {
            foreach (XmlNode node in xmlnode)
            {
                wtTour t = new wtTour();
                t.Id = Convert.ToInt32(node["id"].InnerText);
                t.ssDay = node["ssDay"].InnerText;
                t.Name = node["name"].InnerText;
                t.ssTime = node["ssTime"].InnerText;
                t.Override = Convert.ToInt32(node["override"].InnerText);

                //Tour Scedules Load--start
                if (node.InnerXml.Contains("Schedules"))
                {
                    List<wtTourSchedule> ss = new List<wtTourSchedule>();

                    foreach (XmlNode n in node["Schedules"].ChildNodes)
                    {
                        wtTourSchedule s = new wtTourSchedule();
                        s.DtFrom = n["from"].InnerText;
                        s.DtTo = n["to"].InnerText;

                        int marketid = Convert.ToInt32(n["market"].InnerText);
                        s.Market = markets.Where(x => x.Id == marketid).FirstOrDefault();

                        Dictionary<int, int> d = new Dictionary<int, int>();

                        d.Add(1, Convert.ToInt32(n["d1"].InnerText));
                        d.Add(2, Convert.ToInt32(n["d2"].InnerText));
                        d.Add(3, Convert.ToInt32(n["d3"].InnerText));
                        d.Add(4, Convert.ToInt32(n["d4"].InnerText));
                        d.Add(5, Convert.ToInt32(n["d5"].InnerText));
                        d.Add(6, Convert.ToInt32(n["d6"].InnerText));
                        d.Add(7, Convert.ToInt32(n["d7"].InnerText));

                        s.D = d;

                        ss.Add(s);
                    }

                    t.Schedules = ss;
                }
                //Tour Scedules Load--Stop
                //Tour Time Tables Load--start
                if (node.InnerXml.Contains("TimeTables"))
                {
                    List<wtTourTimeTable> tts = new List<wtTourTimeTable>();

                    foreach (XmlNode n in node["TimeTables"].ChildNodes)
                    {
                        wtTourTimeTable tt = new wtTourTimeTable();
                        tt.Id = Convert.ToInt32(n["id"].InnerText);

                        tt.DtFrom = n["from"].InnerText;
                        if (tt.DtFrom == "") { tt.DtFrom = "01/01/2000"; }

                        tt.DtTo = n["to"].InnerText;
                        if (tt.DtTo == "") { tt.DtTo = "31/12/2099"; }

                        int marketid = Convert.ToInt32(n["market"].InnerText);
                        tt.Market = markets.Where(x => x.Id == marketid).FirstOrDefault();

                        if (node.InnerXml.Contains("PuTimes"))
                        {
                            List<wtTimeTablePUT> ps = new List<wtTimeTablePUT>();

                            foreach (XmlNode n1 in n["PuTimes"].ChildNodes)
                            {
                                wtTimeTablePUT put = new wtTimeTablePUT();
                                put.PointId = Convert.ToInt32(n1["point"].InnerText);
                                put.Time = n1["time"].InnerText;

                                ps.Add(put);
                            }

                            tt.PuTimes = ps;
                        }

                        tts.Add(tt);
                    }

                    t.TimeTables = tts;
                }
                //Tour Time Tables Load--Stop
                //Rates Load -- Start
                if (node.InnerXml.Contains("Rates"))
                {
                    List<wtRate> rs = new List<wtRate>();

                    foreach (XmlNode n in node["Rates"].ChildNodes)
                    {
                        wtRate r = new wtRate();

                        r.DtFrom = (string.IsNullOrEmpty(n["from"].InnerText) ? "01/01/2000" : n["from"].InnerText);
                        r.DtTo = (string.IsNullOrEmpty(n["to"].InnerText) ? "31/12/2099" : n["to"].InnerText);

                        r.adTsfSuppl = Convert.ToDecimal(n["adTsfSuppl"].InnerText, CultureInfo.GetCultureInfo("en"));
                        r.adAddSuppl = Convert.ToDecimal(n["adAddSuppl"].InnerText, CultureInfo.GetCultureInfo("en"));
                        r.chTsfSuppl = Convert.ToDecimal(n["chTsfSuppl"].InnerText, CultureInfo.GetCultureInfo("en"));
                        r.chAddSuppl = Convert.ToDecimal(n["chAddSuppl"].InnerText, CultureInfo.GetCultureInfo("en"));

                        r.CustomerId = Convert.ToInt32(n["customer"].InnerText);
                        r.PriceZone = Convert.ToInt32(n["priceZone"].InnerText);
                        r.RateCode = n["rateCode"].InnerText;

                        r.adRate = Convert.ToDecimal(n["adRate"].InnerText, CultureInfo.GetCultureInfo("en"));
                        r.chRate = Convert.ToDecimal(n["chRate"].InnerText, CultureInfo.GetCultureInfo("en"));

                        rs.Add(r);
                    }

                    t.Rates = rs;
                }

                //Rates Load -- Stop
                allTours.Add(t);
            }
        }

        #endregion


        public class Wrapper<T> : INotifyPropertyChanged
        {
            private T value;
            public T Value
            {
                get { return value; }
                set
                {
                    {
                        this.value = value;
                        OnPropertyChanged();
                    }
                }
            }

            public static implicit operator Wrapper<T>(T value)
            {
                return new Wrapper<T> { value = value };
            }
            public static implicit operator T(Wrapper<T> wrapper)
            {
                return wrapper.value;
            }

            public event PropertyChangedEventHandler PropertyChanged;
            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
