﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using sss.Models;

namespace sss.ViewModels
{
    class SellerViewModel : ViewModel
    {
        public ObservableCollection<wtSeller> AvailableSellers { get; set; }
        wtSeller selectedSeller;

        /// <summary>
        /// Gets or sets the Seller.
        /// </summary>
        public wtSeller SelectedSeller
        {
            get { return this.selectedSeller; }
            set
            {
                this.selectedSeller = value;
                this.AvailableCustomers.Clear();

                if (value != null)
                {
                    foreach (wtCustomer c in this.SelectedSeller.Customers)
                    {
                        AvailableCustomers.Add(c);
                    }

                    if (AvailableCustomers.Count > 1)
                    {
                        AvailableCustomers.Insert(0, new wtCustomer(0, "Select an Agent...", null, null));
                    }

                    foreach (wtTktSeries t in this.selectedSeller.TktSeries)
                    {
                        AvailableTktSeries.Add(t);
                    }
                }


                //this.NotifyPropertyChanged("SelectedSeller");
                this.NotifyPropertyChanged("AllowCustomerSelection");
            }
        }
    }
}
