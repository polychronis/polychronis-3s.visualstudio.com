﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Xml;
using System.Collections.ObjectModel;
using sss.ViewModels;
//using sss.Models;



namespace sss
{
    /// <summary>
    /// Interaction logic for SimpleMessageBox.xaml
    /// </summary>
    public partial class SimpleMessageBox
    {

        public SimpleMessageBox(string title, string message1, string message2)
        {
            InitializeComponent();

            this.Title = title;
            this.Message1.Text = message1;
            this.Message2.Text = message2;
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = true;
            this.Close();
            
        }


    }
}
