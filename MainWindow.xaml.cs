﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Elysium.Theme;
using System.Collections.ObjectModel;
using sss.ViewModels;
using sss.Models;
using System.Xml;
using System.Globalization;

namespace sss
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            Loaded += MainWindow_Loaded;


            //ThemeManager.Instance.Dark(ThemeManager.Instance.Accent);
        }

        //private void LightClick(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    ThemeManager.Instance.Light(ThemeManager.Instance.Accent);
        //}

        //private void DarkClick(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    ThemeManager.Instance.Dark(ThemeManager.Instance.Accent);
        //}

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            DatePicker.BlackoutDates.Add(new CalendarDateRange(new DateTime(), DateTime.Now.AddDays(-1)));
            DatePicker.SelectedDate = DateTime.Now;
            DatePicker.SelectedDateFormat = DatePickerFormat.Short;
        }

        private void Details_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // the original source is what was clicked.  For example 
                // a button.
                DependencyObject dep = (DependencyObject)e.OriginalSource;

                // iteratively traverse the visual tree upwards looking for
                // the clicked row.
                while ((dep != null) && !(dep is DataGridRow))
                {
                    dep = VisualTreeHelper.GetParent(dep);
                }

                // if we found the clicked row
                if (dep != null && dep is DataGridRow)
                {
                    // get the row
                    DataGridRow row = (DataGridRow)dep;

                    // change the details visibility
                    if (row.DetailsVisibility == Visibility.Collapsed)
                    {
                        row.DetailsVisibility = Visibility.Visible;
                    }
                    else
                    {
                        row.DetailsVisibility = Visibility.Collapsed;
                    }
                }
            }
            catch (System.Exception)
            {
            }
        }
    }
}