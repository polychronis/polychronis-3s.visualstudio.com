﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Net.Mail;
using System.Text;
using System.Windows.Controls;
using System.Collections.ObjectModel;

namespace FarmSpace
{
    public class FarmClass
    {

        public static bool isloggedin { get; set; }
        public static string Company { get; set; }
        public static string Season { get; set; }
        public static string UserID { get; set; }
        public static string Username { get; set; }
        public static string CompanyName { get; set; }
        public static string CompanyInfo { get; set; }
        public static string LoginDT { get; set; }
        public static string uMail { get; set; }
        public static List<wtMarket> Markets { get; set; }
        public static List<wtLanguage> Languages { get; set; }
        public static List<wtPuPoint> PuPoints { get; set; }
        public static List<wtAccommodation> Accommodations { get; set; }
        public static List<wtTour> Tours { get; set; }
        public static wtSellers Sellers { get; set; }

        //public static ObservableCollection<wtSeller> ss = new ObservableCollection<wtSeller>();

        public static System.Xml.XmlDocument XMLonHTTP(string input)
        {
            System.Net.WebRequest Request = null;
            Request = System.Net.WebRequest.Create(" http://hq.3strading.gr/cgi-bin/abtwsac.exe/SssWebToursRequestHandler");
            Request.Method = "POST";
            Request.Timeout = 5000;
            XmlDocument doc = new XmlDocument();

            try
            {
                System.DateTime start_time = DateTime.Now;
                System.IO.Stream RequestStream = Request.GetRequestStream();
                System.Text.ASCIIEncoding ASCIIEncoding = new System.Text.ASCIIEncoding();


                byte[] PostData = ASCIIEncoding.GetBytes(input);
                RequestStream.Write(PostData, 0, PostData.Length);
                RequestStream.Close();

                //Dim response As Net.HttpWebResponse = CType(Request.GetResponse(), HttpWebResponse)

                System.Net.WebResponse wr = Request.GetResponse();

                System.IO.Stream iostr = wr.GetResponseStream();

                //string Charset = wr.CharacterSet;
                //Encoding encoding = encoding.GetEncoding(1253);


                System.IO.StreamReader Reader = new System.IO.StreamReader(iostr);


                string ResultHTML = Reader.ReadToEnd();
                doc.LoadXml(AdaptString(ResultHTML));
                TimeSpan elapsed_time = DateTime.Now.Subtract(start_time);
                return doc;
            }
            catch (Exception ex)
            {
                doc.LoadXml(AdaptString("<?xml version=\"1.0\" ?><response>Connection to server Failed</response>"));
                return doc;
            }


            //Return doc
        }

        public static string AdaptString(string s)
        {
            //Removes reserved characters from URL string or HTTP or XML Favorite Descriptionor file
            s = s.Replace("&", "&amp;");
            s = s.Replace("'", "&apos;");

            return s;
        }

        //public static int getPZ(List<wtPuPoint> pup, int id)
        //{
        //    int pz = 0;
        //    foreach (wtPuPoint p in pup)
        //    {
        //        if (p.Id == id)
        //        {
        //            pz = p.PZid;
        //            return pz;
        //        }
        //    }
        //    return pz;
        //}


    }

    public class wtMarket
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class wtLanguage
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class wtPuPoint
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Pzip { get; set; }
    }

    public class wtAccommodation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PuPoint { get; set; }
    }

    public class wtTour
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ssDay { get; set; }
        public string ssTime { get; set; }
        public List<wtTourSchedule> Schedules { get; set; }
        public List<wtTourTimeTable> TimeTables { get; set; }
        public List<wtRate> Rates { get; set; }
        public int Override { get; set; }
    }

    public class wtTourSchedule
    {
        public string DtFrom { get; set; }
        public string DtTo { get; set; }
        public int MarketId { get; set; }
        public Dictionary<int, int> D { get; set; }
    }

    public class wtTourTimeTable
    {
        public int Id { get; set; }
        public string DtFrom { get; set; }
        public string DtTo { get; set; }
        public int MarketId { get; set; }
        public List<wtTimeTablePUT> PuTimes { get; set; }
    }

    public class wtRate
    {
        public string DtFrom { get; set; }
        public string DtTo { get; set; }
        public int adTsfSuppl { get; set; }
        public int adAddSuppl { get; set; }
        public int chTsfSuppl { get; set; }
        public int chAddSuppl { get; set; }
        public int Customer { get; set; }
        public int PriceZone { get; set; }
        public int RateCode { get; set; }
        public decimal adRate { get; set; }
        public decimal chRate { get; set; }
    }

    public class wtTimeTablePUT
    {
        public string Time { get; set; }
        public int PointId { get; set; }
    }

    public class wtSeller
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<wtCustomer> Customers { get; set; }
        public List<wtTktSeries> TktSeries { get; set; }
    }

    
    public class wtCustomer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int LanguageId { get; set; }
        public int MarketId { get; set; }
    }

    public class wtTktSeries
    {
        public int Auto { get; set; }
        public string Code { get; set; }
    }

    public class wtSellers : List<wtSeller>
    {
        public wtSellers()
        {
            if (FarmClass.Sellers != null)
            {
                AddRange(FarmClass.Sellers);
            }
        }
    }

}